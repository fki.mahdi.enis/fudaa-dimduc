/*
 * @file         DimducCourbeMoments.java
 * @creation     2000-05-25
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.Locale;

import org.fudaa.dodico.corba.dimduc.STableauMoments;

import org.fudaa.ebli.graphe.BGraphe;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducCourbeMoments
  extends BGraphe
  implements PropertyChangeListener {
  private NumberFormat nf0_, nf2_;
  private STableauMoments[] tm_;
  private String GRA_TITRE;
  private String GRA_ABS_LABEL;
  private String GRA_ORD_LABEL;
  private String GRA_ABS_UNITE;
  private String GRA_ORD_UNITE;
  public DimducCourbeMoments(
    final STableauMoments[] _tm,
    final String gra_titre,
    final String gra_abs_label,
    final String gra_ord_label,
    final String gra_abs_unite,
    final String gra_ord_unite) {
    super();
    GRA_TITRE= gra_titre;
    GRA_ABS_LABEL= gra_abs_label;
    GRA_ORD_LABEL= gra_ord_label;
    GRA_ABS_UNITE= gra_abs_unite;
    GRA_ORD_UNITE= gra_ord_unite;
    nf0_= NumberFormat.getInstance(Locale.US);
    nf0_.setMaximumFractionDigits(0);
    nf0_.setGroupingUsed(false);
    nf2_= NumberFormat.getInstance(Locale.US);
    nf2_.setMaximumFractionDigits(3);
    nf2_.setGroupingUsed(false);
    tm_= _tm;
    setPreferredSize(new Dimension(640, 480));
    init();
  }
  private void init() {
    if (tm_ == null) {
      return;
    }
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      } // fin if
    } // fin try
    catch (final IOException ex) {
      System.err.println(ex);
    }
  } // fin void init ()
  public void setCourbeMoments(final STableauMoments[] t) {
    if (t == tm_) {
      return;
    }
    final STableauMoments[] vp= tm_;
    tm_= t;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("courbe_moments", vp, tm_);
  } // fin void setCourbeMoments
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("courbe_moments".equals(e.getPropertyName())) {
      final STableauMoments[] nv= (STableauMoments[])e.getNewValue();
      setCourbeMoments(nv);
      init();
    }
  }
  private InputStream generateGDF() throws IOException {
    if ((tm_ == null)) {
      return null;
    }
    final int nbPoint= tm_.length;
    if (nbPoint <= 0) {
      return null;
    }
    String gdfStr= "";
    final String crbTitre= "Courbe des moments ";
    String crbStr= "";
    //calage de la courbe avec recherche des minis et des maxis :
    // initialisation :  si pas de point ? :
    double minMomentVal= -10000.0, maxMomentVal= 0.0;
    double minCoteVal= -50.0, maxCoteVal= 0.0;
    // initialisation avec pt 0 :
    if (nbPoint > 0) {
      minMomentVal= maxMomentVal= tm_[0].valeurMoment;
      minCoteVal= maxCoteVal= tm_[0].coteMoment;
    } // fin if( nbPoint>0 )
    // recherche sur l'ensemble des valeurs :
    for (int i= 0; i < nbPoint; i++) {
      if (minMomentVal > tm_[i].valeurMoment) {
        minMomentVal= tm_[i].valeurMoment;
      }
      if (maxMomentVal < tm_[i].valeurMoment) {
        maxMomentVal= tm_[i].valeurMoment;
      }
      if (minCoteVal > tm_[i].coteMoment) {
        minCoteVal= tm_[i].coteMoment;
      }
      if (maxCoteVal < tm_[i].coteMoment) {
        maxCoteVal= tm_[i].coteMoment;
      }
      // alimentation chaine support graphique par valeurs points de la courbe :
      crbStr += "      "
        + nf0_.format(tm_[i].valeurMoment)
        + " "
        + nf2_.format(tm_[i].coteMoment)
        + "\n";
    } // fin boucle for
    // encadrement des zones de graduation des cotes par des valeurs enti�res :
    final int minCoteVal_Int= (int) (minCoteVal) - 1;
    minCoteVal= (minCoteVal_Int);
    final int maxCoteVal_Int= (int) (maxCoteVal) + 1;
    maxCoteVal= (maxCoteVal_Int);
    // le graphe :
    gdfStr += "graphe\n{\n";
    gdfStr += "  titre \"" + crbTitre + "\n";
    gdfStr += "  animation non\n";
    gdfStr += "  legende oui\n";
    gdfStr += "  marges\n  {\n";
    gdfStr += "    gauche 60\n";
    gdfStr += "    droite 60\n";
    gdfStr += "    haut   45\n";
    gdfStr += "    bas    30\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"" + GRA_ABS_LABEL + "\"\n";
    gdfStr += "    unite \"" + GRA_ABS_UNITE + "\"\n";
    gdfStr += "    orientation horizontal\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf2_.format(minMomentVal) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxMomentVal) + "\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"" + GRA_ORD_LABEL + "\"\n";
    gdfStr += "    unite \"" + GRA_ORD_UNITE + "\"\n";
    gdfStr += "    orientation vertical\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf0_.format(minCoteVal) + "\n";
    gdfStr += "    maximum " + nf0_.format(maxCoteVal) + "\n  }\n";
    gdfStr += "  courbe\n  {\n";
    gdfStr += "    titre \"" + GRA_TITRE + "\"\n";
    gdfStr += "    marqueurs oui\n";
    gdfStr += "    valeurs\n    {\n";
    gdfStr += crbStr;
    gdfStr += "    }\n  }\n";
    return new ByteArrayInputStream(gdfStr.getBytes());
  }
}
