/*
 * @file         DimducPanneauDuc.java
 * @creation     2001-01-25
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.dimduc.SDucAlbeSansDefense;
import org.fudaa.dodico.corba.dimduc.SSollicitationAccostage;
import org.fudaa.dodico.corba.ducalbe.SEnsembleTubes;
import org.fudaa.dodico.corba.ducalbe.STroncon;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducPanneauDuc extends BuInternalFrame {
  JComponent content= null;
  JTabbedPane tpMain= null;
  protected SSollicitationAccostage[] resultatsD= null;
  protected SDucAlbeSansDefense albe_ss_def= null;
  protected SEnsembleTubes ensemble_tubes= null;
  protected STroncon tube= null;
  int n;
  DimducArrondis arrondi_;
  String unites_poids_= "tonnes";
  String unites_moments_= "tonnes.m�tres";
  String unites_energies_= "'tonnes.m�tres'";
  BuTextField tf_poids_, tf_diametre_, tf_epaisseur_, tf_epaisseur_cor_;
  BuTextField tf_fiche_,
    tf_cote_pied_,
    tf_moment_max_,
    tf_cote_moment_max_,
    tf_reaction_max_;
  BuTextField tf_fleche_choc_, tf_cote_choc_, tf_fleche_tete_, tf_cote_tete_;
  BuTextField tf_energie_pieux_, tf_energie_defense_, tf_energie_totale_;
  double poids_, diametre_, epaisseur_, epaisseur_cor_;
  double fiche_, cote_pied_, moment_max_, cote_moment_max_, reaction_max_;
  double fleche_choc_, cote_choc_, fleche_tete_, cote_tete_;
  double energie_pieux_, energie_defense_, energie_totale_;
  /**
   * Constructeur de la fenetre interne.
   */
  public DimducPanneauDuc(
    final SSollicitationAccostage[] _resultatsD,
    final String titre,
    final int no) {
    super("", true, true, true, true);
    // creation de la table des onglets
    tpMain= new JTabbedPane();
    // panneau duc le plus leger :  6 colonnes
    final JPanel pnResultatsD= new JPanel();
    final BuGridLayout lo02= new BuGridLayout();
    lo02.setColumns(6);
    lo02.setHfilled(true);
    lo02.setHgap(5);
    lo02.setVgap(5);
    pnResultatsD.setLayout(lo02);
    pnResultatsD.setBorder(new EmptyBorder(5, 5, 5, 5));
    // instanciations :
    albe_ss_def= _resultatsD[no].ducAlbe.ducAlbeSansDefense; // pointeur nul
    ensemble_tubes= albe_ss_def.ensembleTubes;
    tube= ensemble_tubes.tubes[0].tube[0];
    // declaration champs :
    // poids :
    tf_poids_= BuTextField.createDoubleField();
    poids_= tube.poidsTroncon;
    arrondi_= new DimducArrondis(poids_);
    arrondi_.arrondi2Decimales();
    tf_poids_.setText("" + arrondi_.arrondi());
    tf_poids_.setColumns(6);
    // epaisseur :
    tf_epaisseur_= BuTextField.createDoubleField();
    epaisseur_= tube.epaisseurTroncon.epaisseur;
    arrondi_= new DimducArrondis(epaisseur_);
    arrondi_.arrondi2Decimales();
    tf_epaisseur_.setText("" + arrondi_.arrondi());
    tf_epaisseur_.setColumns(6);
    // diametre :
    tf_diametre_= BuTextField.createDoubleField();
    diametre_= tube.diametreTroncon.diametre;
    arrondi_= new DimducArrondis(diametre_);
    arrondi_.arrondi2Decimales();
    tf_diametre_.setText("" + arrondi_.arrondi());
    tf_diametre_.setColumns(6);
    // epaisseur corrodee :
    tf_epaisseur_cor_= BuTextField.createDoubleField();
    epaisseur_cor_= tube.epaisseurCorrodee;
    arrondi_= new DimducArrondis(epaisseur_cor_);
    arrondi_.arrondi2Decimales();
    tf_epaisseur_cor_.setText("" + arrondi_.arrondi());
    tf_epaisseur_cor_.setColumns(6);
    // fiche :
    tf_fiche_= BuTextField.createDoubleField();
    fiche_= albe_ss_def.coteCbutee;
    arrondi_= new DimducArrondis(fiche_);
    arrondi_.arrondi2Decimales();
    tf_fiche_.setText("" + arrondi_.arrondi());
    tf_fiche_.setColumns(6);
    // cote pied :
    tf_cote_pied_= BuTextField.createDoubleField();
    cote_pied_= albe_ss_def.cotePied;
    arrondi_= new DimducArrondis(cote_pied_);
    arrondi_.arrondi2Decimales();
    tf_cote_pied_.setText("" + arrondi_.arrondi());
    tf_cote_pied_.setColumns(6);
    // moment max :
    tf_moment_max_= BuTextField.createDoubleField();
    moment_max_= albe_ss_def.momentMaxi;
    arrondi_= new DimducArrondis(moment_max_);
    arrondi_.arrondi2Decimales();
    tf_moment_max_.setText("" + arrondi_.arrondi());
    tf_moment_max_.setColumns(6);
    // cote moment max :
    tf_cote_moment_max_= BuTextField.createDoubleField();
    cote_moment_max_= albe_ss_def.coteMomentMaxi;
    arrondi_= new DimducArrondis(cote_moment_max_);
    arrondi_.arrondi2Decimales();
    tf_cote_moment_max_.setText("" + arrondi_.arrondi());
    tf_cote_moment_max_.setColumns(6);
    // reaction max :
    tf_reaction_max_= BuTextField.createDoubleField();
    reaction_max_= albe_ss_def.forceMaxi;
    arrondi_= new DimducArrondis(reaction_max_);
    arrondi_.arrondi2Decimales();
    tf_reaction_max_.setText("" + arrondi_.arrondi());
    tf_reaction_max_.setColumns(6);
    // fleche au choc :
    tf_fleche_choc_= BuTextField.createDoubleField();
    fleche_choc_= albe_ss_def.flecheAuChoc;
    arrondi_= new DimducArrondis(fleche_choc_);
    arrondi_.arrondi2Decimales();
    tf_fleche_choc_.setText("" + arrondi_.arrondi());
    tf_fleche_choc_.setColumns(6);
    // cote au choc :
    tf_cote_choc_= BuTextField.createDoubleField();
    cote_choc_= albe_ss_def.coteChoc;
    arrondi_= new DimducArrondis(cote_choc_);
    arrondi_.arrondi2Decimales();
    tf_cote_choc_.setText("" + arrondi_.arrondi());
    tf_cote_choc_.setColumns(6);
    // fleche en tete :
    tf_fleche_tete_= BuTextField.createDoubleField();
    fleche_tete_= albe_ss_def.flecheEnTete;
    arrondi_= new DimducArrondis(fleche_tete_);
    arrondi_.arrondi2Decimales();
    tf_fleche_tete_.setText("" + arrondi_.arrondi());
    tf_fleche_tete_.setColumns(6);
    // cote en tete :
    tf_cote_tete_= BuTextField.createDoubleField();
    cote_tete_= albe_ss_def.coteTete;
    arrondi_= new DimducArrondis(cote_tete_);
    arrondi_.arrondi2Decimales();
    tf_cote_tete_.setText("" + arrondi_.arrondi());
    tf_cote_tete_.setColumns(6);
    // energies :
    tf_energie_totale_= BuTextField.createDoubleField();
    energie_totale_= _resultatsD[no].ducAlbe.energieDucAlbe;
    arrondi_= new DimducArrondis(energie_totale_);
    arrondi_.arrondi2Decimales();
    tf_energie_totale_.setText("" + arrondi_.arrondi());
    tf_energie_totale_.setColumns(6);
    tf_energie_defense_= BuTextField.createDoubleField();
    energie_defense_= _resultatsD[no].ducAlbe.defense.energieDefense;
    arrondi_= new DimducArrondis(energie_defense_);
    arrondi_.arrondi2Decimales();
    tf_energie_defense_.setText("" + arrondi_.arrondi());
    tf_energie_defense_.setColumns(6);
    tf_energie_pieux_= BuTextField.createDoubleField();
    energie_pieux_= energie_totale_ - energie_defense_;
    arrondi_= new DimducArrondis(energie_pieux_);
    arrondi_.arrondi2Decimales();
    tf_energie_pieux_.setText("" + arrondi_.arrondi());
    tf_energie_pieux_.setColumns(6);
    // construction panneau :
    // ligne blanche :
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Poids : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_poids_, n++);
    pnResultatsD.add(new JLabel(unites_poids_), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Epaisseur : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_epaisseur_, n++);
    pnResultatsD.add(new JLabel("mm"), n++);
    pnResultatsD.add(new JLabel("Diam�tre : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_diametre_, n++);
    pnResultatsD.add(new JLabel("mm", SwingConstants.LEFT), n++);
    pnResultatsD.add(new JLabel("Epaisseur corrod�e : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_epaisseur_cor_, n++);
    pnResultatsD.add(new JLabel("mm"), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Fiche : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_fiche_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel("Cote pied : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_cote_pied_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Moment maxi : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_moment_max_, n++);
    pnResultatsD.add(new JLabel(unites_moments_), n++);
    pnResultatsD.add(new JLabel("� la cote : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_cote_moment_max_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel("R�action maxi : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_reaction_max_, n++);
    pnResultatsD.add(new JLabel(unites_poids_), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Fleche au choc : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_fleche_choc_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel("� la cote : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_cote_choc_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel("Fleche en tete : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_fleche_tete_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel("� la cote : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_cote_tete_, n++);
    pnResultatsD.add(new JLabel("m"), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Energies en ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(new JLabel(unites_energies_ + " : ", SwingConstants.LEFT), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel(" "), n++);
    pnResultatsD.add(new JLabel("Pieux : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_energie_pieux_, n++);
    pnResultatsD.add(new JLabel("D�fense : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_energie_defense_, n++);
    pnResultatsD.add(new JLabel("Totale : ", SwingConstants.RIGHT), n++);
    pnResultatsD.add(tf_energie_totale_, n++);
    // ********************************
    final JPanel cdResultatsD= new JPanel();
    cdResultatsD.setLayout(new BorderLayout());
    cdResultatsD.add("North", pnResultatsD);
    tpMain.addTab(titre, null, pnResultatsD, titre);
    content= (JComponent)getContentPane();
    content.setBorder(new EmptyBorder(5, 5, 5, 5));
    content.add("Center", tpMain);
    setTitle("Consultation des R�sultats");
    setLocation(40, 40);
    pack();
  } // fin constructeur
} // fin classe DimducPanneauDuc
