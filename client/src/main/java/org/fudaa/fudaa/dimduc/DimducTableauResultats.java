/*
 * @file         DimducTableauResultats.java
 * @creation     1999-12-20
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;

import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableSortModel;

import org.fudaa.dodico.corba.dimduc.SSollicitationAccostage;
import org.fudaa.dodico.corba.dimduc.STableauMoments;
import org.fudaa.dodico.corba.ducalbe.SPointEffortDeformation;

// classe DimducTableauResultatsModelDC :
class DimducTableauResultatsModelDC implements TableModel {
  Vector listeners_;

  int mode = 1;

  SSollicitationAccostage[] resultatsDC_;

  public DimducTableauResultatsModelDC(final SSollicitationAccostage[] _resultats) {
    listeners_ = new Vector();
    resultatsDC_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return (Double.class);
  }

  public int getColumnCount() {
    return 16;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "poids (t)";
      break;
    case 1:
      r = "d.(mm)";
      break;
    case 2:
      r = "ep.(mm)";
      break;
    case 3:
      r = "ep. cor.(mm)";
      break;
    case 4:
      r = "contrainte (tm)";
      break;
    case 5:
      r = "cote acc. (m)";
      break;
    case 6:
      r = "energie (tm)";
      break;
    case 7:
      r = "R adm.(t)";
      break;
    case 8:
      r = "fiche (m)";
      break;
    case 9:
      r = "cote pied(m)";
      break;
    case 10:
      r = "cote tete(m)";
      break;
    case 11:
      r = "fleche choc(m)";
      break;
    case 12:
      r = "fleche tete(m)";
      break;
    case 13:
      r = "energie pieux (tm)";
      break;
    case 14:
      r = "energie defense (tm)";
      break;
    case 15:
      r = "energie totale (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return 1;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    switch (column) {
    case 0:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].poidsTroncon);
      break;
    case 1:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon.diametre);
      break;
    case 2:
      r = new Double(
          resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon.epaisseur);
      break;
    case 3:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurCorrodee);
      break;
    case 4:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes.contrainteAdm);
      break;
    case 5:
      r = new Double(resultatsDC_[1].accostage.coteAccostage);
      break;
    case 6:
      r = new Double(resultatsDC_[1].accostage.energieAccostage);
      break;
    case 7:
      r = new Double(resultatsDC_[1].accostage.reactionAdmissible);
      break;
    case 8:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.coteCbutee);
      break;
    case 9:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.cotePied);
      break;
    case 10:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.coteTete);
      break;
    case 11:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.flecheAuChoc);
      break;
    case 12:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.flecheEnTete);
      break;
    case 13:
      r = new Double(resultatsDC_[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.energieTubes);
      break;
    case 14:
      r = new Double(resultatsDC_[1].ducAlbe.defense.energieDefense);
      break;
    case 15:
      r = new Double(resultatsDC_[1].ducAlbe.energieDucAlbe);
      break;
    }
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelDC
// classe DimducTableauResultatsModelDL :

class DimducTableauResultatsModelDL implements TableModel {
  Vector listeners_;

  int mode = 0;

  SSollicitationAccostage[] resultatsDL_;

  public DimducTableauResultatsModelDL(final SSollicitationAccostage[] _resultats) {
    listeners_ = new Vector();
    resultatsDL_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return (Double.class);
  }

  public int getColumnCount() {
    return 16;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "poids (t)";
      break;
    case 1:
      r = "d.(mm)";
      break;
    case 2:
      r = "ep.(mm)";
      break;
    case 3:
      r = "ep. cor.(mm)";
      break;
    case 4:
      r = "contrainte (tm)";
      break;
    case 5:
      r = "cote acc. (m)";
      break;
    case 6:
      r = "energie (tm)";
      break;
    case 7:
      r = "R adm.(t)";
      break;
    case 8:
      r = "fiche (m)";
      break;
    case 9:
      r = "cote pied(m)";
      break;
    case 10:
      r = "cote tete(m)";
      break;
    case 11:
      r = "fleche choc(m)";
      break;
    case 12:
      r = "fleche tete(m)";
      break;
    case 13:
      r = "energie pieux (tm)";
      break;
    case 14:
      r = "energie defense (tm)";
      break;
    case 15:
      r = "energie totale (tm)";
      break;
    } // fin switch
    return r;
  }

  public int getRowCount() {
    return 1;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    switch (column) {
    case 0:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].poidsTroncon);
      break;
    case 1:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon.diametre);
      break;
    case 2:
      r = new Double(
          resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon.epaisseur);
      break;
    case 3:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurCorrodee);
      break;
    case 4:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes.contrainteAdm);
      break;
    case 5:
      r = new Double(resultatsDL_[0].accostage.coteAccostage);
      break;
    case 6:
      r = new Double(resultatsDL_[0].accostage.energieAccostage);
      break;
    case 7:
      r = new Double(resultatsDL_[0].accostage.reactionAdmissible);
      break;
    case 8:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.coteCbutee);
      break;
    case 9:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.cotePied);
      break;
    case 10:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.coteTete);
      break;
    case 11:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.flecheAuChoc);
      break;
    case 12:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.flecheEnTete);
      break;
    case 13:
      r = new Double(resultatsDL_[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.energieTubes);
      break;
    case 14:
      r = new Double(resultatsDL_[0].ducAlbe.defense.energieDefense);
      break;
    case 15:
      r = new Double(resultatsDL_[0].ducAlbe.energieDucAlbe);
      break;
    }
    return r;
  }



  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelDL
// classe DimducTableauResultatsModelMC :

class DimducTableauResultatsModelMC implements TableModel {
  Vector listeners_;

  int mode = 4;

  STableauMoments[] resultatsMC_;

  public DimducTableauResultatsModelMC(final STableauMoments[] _resultats) {
    listeners_ = new Vector();
    resultatsMC_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return (Double.class);
  }

  public int getColumnCount() {
    return 2;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "cote   (m)";
      break;
    case 1:
      r = "valeur (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return resultatsMC_.length;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    switch (column) {
    case 0:
      r = new Double(resultatsMC_[row].coteMoment);
      break;
    case 1:
      r = new Double(resultatsMC_[row].valeurMoment);
      break;
    }
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelMC
// classe DimducTableauResultatsModelML :

class DimducTableauResultatsModelML implements TableModel {
  Vector listeners_;

  int mode = 3;

  STableauMoments[] resultatsML_;

  public DimducTableauResultatsModelML(final STableauMoments[] _resultats) {
    listeners_ = new Vector();
    resultatsML_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return Double.class;
  }

  public int getColumnCount() {
    return 2;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "cote   (m)";
      break;
    case 1:
      r = "valeur (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return resultatsML_.length;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    switch (column) {
    case 0:
      r = new Double(resultatsML_[row].coteMoment);
      break;
    case 1:
      r = new Double(resultatsML_[row].valeurMoment);
      break;
    }
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelML
// classe DimducTableauResultatsModelPEDC :

class DimducTableauResultatsModelPEDC implements TableModel {
  Vector listeners_;

  int mode = 7; // non utilise

  SPointEffortDeformation[] pedc_;

  public DimducTableauResultatsModelPEDC(final SPointEffortDeformation[] _resultats) {
    listeners_ = new Vector();
    pedc_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return (Double.class);
  }

  public int getColumnCount() {
    return 2;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "deformation (%)";
      break;
    case 1:
      r = "effort (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return pedc_.length;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    System.out.println(" pedc_.length  : nb points = " + pedc_.length);
    switch (column) {
    case 0:
      r = new Double(pedc_[row].deformationDefense); // pointeur nul
      //System.out.println(" deformation n� " + (row + 1) + " = " + r);
      break;
    case 1:
      r = new Double(pedc_[row].effortDefense);
      //System.out.println(" effort n� " + (row + 1) + " = " + r);
      break;
    } // fin switch
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelPEDC
// classe DimducTableauResultatsModelPEDL :

class DimducTableauResultatsModelPEDL implements TableModel {
  Vector listeners_;

  int mode = 6; // non utilise

  SPointEffortDeformation[] pedl_;

  public DimducTableauResultatsModelPEDL(final SPointEffortDeformation[] _resultats) {
    listeners_ = new Vector();
    pedl_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return Double.class;
  }

  public int getColumnCount() {
    return 2;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "deformation (%)";
      break;
    case 1:
      r = "effort (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return pedl_.length;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    //System.out.println(" pedl_.length  : nb points = " + pedl_.length);
    switch (column) {
    case 0:
      r = new Double(pedl_[row].deformationDefense);
      //System.out.println(" deformation n� " + (row + 1) + " = " + r);
      break;
    case 1:
      r = new Double(pedl_[row].effortDefense);
      //System.out.println(" effort n� " + (row + 1) + " = " + r);
      break;
    } // fin switch
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelPEDL
// ***********************************
// class DimducTableauResultatsModelTS

class DimducTableauResultatsModelTS implements TableModel {
  Vector listeners_;

  int mode = 2;

  SSollicitationAccostage[] resultatsTS_;

  public DimducTableauResultatsModelTS(final SSollicitationAccostage[] _resultats) {
    listeners_ = new Vector();
    resultatsTS_ = _resultats;
  }

  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }

  public Class getColumnClass(final int column) {
    return (column == 0 ? Integer.class : Double.class);
  }

  public int getColumnCount() {
    return 16;
  }

  public String getColumnName(final int column) {
    String r = "";
    switch (column) {
    case 0:
      r = "poids (t)";
      break;
    case 1:
      r = "d.(mm)";
      break;
    case 2:
      r = "ep.(mm)";
      break;
    case 3:
      r = "ep. cor.(mm)";
      break;
    case 4:
      r = "contrainte (tm)";
      break;
    case 5:
      r = "cote acc. (m)";
      break;
    case 6:
      r = "energie (tm)";
      break;
    case 7:
      r = "R adm.(t)";
      break;
    case 8:
      r = "fiche (m)";
      break;
    case 9:
      r = "cote pied(m)";
      break;
    case 10:
      r = "cote tete(m)";
      break;
    case 11:
      r = "fleche choc(m)";
      break;
    case 12:
      r = "fleche tete(m)";
      break;
    case 13:
      r = "energie pieux (tm)";
      break;
    case 14:
      r = "energie defense (tm)";
      break;
    case 15:
      r = "energie totale (tm)";
      break;
    }
    return r;
  }

  public int getRowCount() {
    return resultatsTS_.length;
  }

  public Object getValueAt(final int row, final int column) {
    Object r = null;
    switch (column) {
    case 0:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].poidsTroncon);
      break;
    case 1:
      r = new Double(
          resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon.diametre);
      break;
    case 2:
      r = new Double(
          resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon.epaisseur);
      break;
    case 3:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurCorrodee);
      break;
    case 4:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes.contrainteAdm);
      break;
    case 5:
      r = new Double(resultatsTS_[row].accostage.coteAccostage);
      break;
    case 6:
      r = new Double(resultatsTS_[row].accostage.energieAccostage);
      break;
    case 7:
      r = new Double(resultatsTS_[row].accostage.reactionAdmissible);
      break;
    case 8:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.coteCbutee);
      break;
    case 9:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.cotePied);
      break;
    case 10:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.coteTete);
      break;
    case 11:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.flecheAuChoc);
      break;
    case 12:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.flecheEnTete);
      break;
    case 13:
      r = new Double(resultatsTS_[row].ducAlbe.ducAlbeSansDefense.ensembleTubes.energieTubes);
      break;
    case 14:
      r = new Double(resultatsTS_[row].ducAlbe.defense.energieDefense);
      break;
    case 15:
      r = new Double(resultatsTS_[row].ducAlbe.energieDucAlbe);
      break;
    }
    return r;
  }

  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }

  public void setValueAt(final Object value, final int row, final int column) {}
} // fin classe DimducTableauResultatsModelTS

/**
 * Tableau des resultats.
 *
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author Christian Barou
 */
public class DimducTableauResultats extends CtuluTable {
  // PEDC
  private SPointEffortDeformation[] pedc_;

  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Proprietes resultats
  // PEDL
  private SPointEffortDeformation[] pedl_;

  // DC
  private SSollicitationAccostage[] sollicitationAccostagesDC_;

  // DL
  private SSollicitationAccostage[] sollicitationAccostagesDL_;

  // TS
  private SSollicitationAccostage[] sollicitationAccostagesTS_;

  // MC
  private STableauMoments[] tableauMomentsCourt_;

  // ML
  private STableauMoments[] tableauMomentsLeger_;

  int mode = 0;

  public DimducTableauResultats() {
    super();
    mode = 0;
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  } // fin constructeur DimducTableauResultats

  private void reinitialiseDC() {
    if (sollicitationAccostagesDC_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelDC(sollicitationAccostagesDC_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(30); // poids
    getColumn(getColumnName(1)).setWidth(45); // diametre
    getColumn(getColumnName(2)).setWidth(45); // epaisseur
    getColumn(getColumnName(3)).setWidth(45); // epaisseur cor.
    getColumn(getColumnName(4)).setWidth(57); // contrainte
    getColumn(getColumnName(5)).setWidth(40); // cote accostage
    getColumn(getColumnName(6)).setWidth(45); // energie accostage
    getColumn(getColumnName(7)).setWidth(45); // reaction accostage
    getColumn(getColumnName(8)).setWidth(35); // fiche
    getColumn(getColumnName(9)).setWidth(40); // cote pied
    getColumn(getColumnName(10)).setWidth(40); // cote tete
    getColumn(getColumnName(11)).setWidth(52); // fleche choc
    getColumn(getColumnName(12)).setWidth(50); // fleche en tete
    getColumn(getColumnName(13)).setWidth(75); // energie pieux
    getColumn(getColumnName(14)).setWidth(75); // energie defense
    getColumn(getColumnName(15)).setWidth(75); // energie totale
  }

  private void reinitialiseDL() {
    if (sollicitationAccostagesDL_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelDL(sollicitationAccostagesDL_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(30); // poids
    getColumn(getColumnName(1)).setWidth(45); // diametre
    getColumn(getColumnName(2)).setWidth(45); // epaisseur
    getColumn(getColumnName(3)).setWidth(45); // epaisseur cor.
    getColumn(getColumnName(4)).setWidth(57); // contrainte
    getColumn(getColumnName(5)).setWidth(40); // cote accostage
    getColumn(getColumnName(6)).setWidth(45); // energie accostage
    getColumn(getColumnName(7)).setWidth(45); // reaction accostage
    getColumn(getColumnName(8)).setWidth(35); // fiche
    getColumn(getColumnName(9)).setWidth(40); // cote pied
    getColumn(getColumnName(10)).setWidth(40); // cote tete
    getColumn(getColumnName(11)).setWidth(52); // fleche choc
    getColumn(getColumnName(12)).setWidth(50); // fleche en tete
    getColumn(getColumnName(13)).setWidth(75); // energie pieux
    getColumn(getColumnName(14)).setWidth(75); // energie defense
    getColumn(getColumnName(15)).setWidth(75); // energie totale
  }

  private void reinitialiseMC() {
    if (tableauMomentsCourt_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelMC(tableauMomentsCourt_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(40); // cote moment
    getColumn(getColumnName(1)).setWidth(100); // valeur moment
  } // fin void reinitialiseMC

  private void reinitialiseML() {
    if (tableauMomentsLeger_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelML(tableauMomentsLeger_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(40); // cote moment
    getColumn(getColumnName(1)).setWidth(100); // valeur moment
  }

  boolean modelIsInstalled_;

  public void setModel(final TableModel _dataModel) {
    if (dataModel == null) {
      dataModel = new CtuluTableSortModel();
    }
    ((CtuluTableSortModel) dataModel).setModel(_dataModel);
    if (getColumnModel() != null) {
      tableChanged(new TableModelEvent(dataModel, TableModelEvent.HEADER_ROW));
    }
    if(!modelIsInstalled_ && getTableHeader()!=null ){
      if (_dataModel != null) {
        ((CtuluTableSortModel) dataModel).install(this);
      }
      modelIsInstalled_=true;
    }
    firePropertyChange("model", null, dataModel);
  }

  private void reinitialisePEDC() {
    if (pedc_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelPEDC(pedc_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(50); // deformation
    getColumn(getColumnName(1)).setWidth(100); // effort
  } // fin void reinitialise PEDC

  private void reinitialisePEDL() {
    if (pedl_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelPEDL(pedl_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    getColumn(getColumnName(0)).setWidth(50); // deformation
    getColumn(getColumnName(1)).setWidth(100); // effort
  } // fin void reinitialise PEDL

  private void reinitialiseTS() {
    if (sollicitationAccostagesTS_ == null) {
      return;
    }
    setModel(new DimducTableauResultatsModelTS(sollicitationAccostagesTS_));
    final BuTableCellRenderer tcr = new BuTableCellRenderer();
    final NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i = 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(0)).setWidth(30); // poids
    }
    getColumn(getColumnName(1)).setWidth(45); // diametre
    getColumn(getColumnName(2)).setWidth(45); // epaisseur
    getColumn(getColumnName(3)).setWidth(45); // epaisseur cor.
    getColumn(getColumnName(4)).setWidth(57); // contrainte
    getColumn(getColumnName(5)).setWidth(40); // cote accostage
    getColumn(getColumnName(6)).setWidth(45); // energie accostage
    getColumn(getColumnName(7)).setWidth(45); // reaction accostage
    getColumn(getColumnName(8)).setWidth(35); // fiche
    getColumn(getColumnName(9)).setWidth(40); // cote pied
    getColumn(getColumnName(10)).setWidth(40); // cote tete
    getColumn(getColumnName(11)).setWidth(52); // fleche choc
    getColumn(getColumnName(12)).setWidth(50); // fleche en tete
    getColumn(getColumnName(13)).setWidth(75); // energie pieux
    getColumn(getColumnName(14)).setWidth(75); // energie defense
    getColumn(getColumnName(15)).setWidth(75); // energie totale
  } // fin void reinitialiseTS()

  SSollicitationAccostage[] getResultatsDC() {
    return sollicitationAccostagesDC_;
  }

  SSollicitationAccostage[] getResultatsDL() {
    return sollicitationAccostagesDL_;
  }

  STableauMoments[] getResultatsMC() {
    return tableauMomentsCourt_;
  }

  STableauMoments[] getResultatsML() {
    return tableauMomentsLeger_;
  }

  SPointEffortDeformation[] getResultatsPEDC() {
    return pedc_;
  }

  SPointEffortDeformation[] getResultatsPEDL() {
    return pedl_;
  }

  SSollicitationAccostage[] getResultatsTS() {
    return sollicitationAccostagesTS_;
  }

  public void reinitialise() // pas d'appel
  {
    reinitialiseDL();
    reinitialiseDC();
    reinitialiseTS();
    reinitialiseML();
    reinitialiseMC();
    reinitialisePEDL();
    reinitialisePEDC();
  }

  public void setMode(final int m) {
    mode = m;
    System.out.println(" mode = " + mode);
    switch (mode) {
    case 0:
      reinitialiseDL();
      break;
    case 1:
      reinitialiseDC();
      break;
    case 2:
      reinitialiseTS();
      break;
    case 3:
      reinitialiseML();
      break;
    case 4:
      reinitialiseMC();
      break;
    case 6:
      reinitialisePEDL();
      break;
    case 7:
      reinitialisePEDC();
      break;
    } // fin switch
  }

  public void setResultatsDC(final SSollicitationAccostage[] _resultats) {
    final SSollicitationAccostage[] dc = sollicitationAccostagesDC_;
    sollicitationAccostagesDC_ = _resultats;
    reinitialiseDC();
    repaint();
    firePropertyChange("resultatsDC", dc, sollicitationAccostagesDC_);
  } // fin void setResultatsDC

  public void setResultatsDL(final SSollicitationAccostage[] _resultats) {
    final SSollicitationAccostage[] dl = sollicitationAccostagesDL_;
    sollicitationAccostagesDL_ = _resultats;
    reinitialiseDL();
    repaint();
    firePropertyChange("resultatsDL", dl, sollicitationAccostagesDL_);
  } // fin void setResultatsDL

  public void setResultatsMC(final STableauMoments[] _resultats) {
    final STableauMoments[] mc = tableauMomentsCourt_;
    tableauMomentsCourt_ = _resultats;
    reinitialiseMC();
    repaint();
    firePropertyChange("resultatsMC", mc, tableauMomentsCourt_);
  } // fin void setResultatsMC

  public void setResultatsML(final STableauMoments[] _resultats) {
    final STableauMoments[] ml = tableauMomentsLeger_;
    tableauMomentsLeger_ = _resultats;
    reinitialiseML();
    repaint();
    firePropertyChange("resultatsML", ml, tableauMomentsLeger_);
  } // fin void setResultatsML

  public void setResultatsPEDC(final SPointEffortDeformation[] _resultats) {
    final SPointEffortDeformation[] pedc = pedc_;
    pedc_ = _resultats;
    reinitialisePEDC();
    repaint();
    firePropertyChange("resultatsPEDC", pedc, pedc_);
  } // fin void setResultatsPEDC

  public void setResultatsPEDL(final SPointEffortDeformation[] _resultats) {
    final SPointEffortDeformation[] pedl = pedl_;
    pedl_ = _resultats;
    reinitialisePEDL();
    repaint();
    firePropertyChange("resultatsPEDL", pedl, pedl_);
  } // fin void setResultatsPEDL

  public void setResultatsTS(final SSollicitationAccostage[] _resultats) {
    final SSollicitationAccostage[] ts = sollicitationAccostagesTS_;
    sollicitationAccostagesTS_ = _resultats;
    reinitialiseTS();
    repaint();
    firePropertyChange("resultatsTS", ts, sollicitationAccostagesTS_);
  } // fin void setResultatsTS
} // fin classe DimducTableauResultats
