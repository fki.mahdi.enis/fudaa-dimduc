/*
 * @file         DimducPreferencesPanel.java
 * @creation     1999-12-21
 * @modification $Date: 2006-09-19 15:02:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.util.Hashtable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
/**
 * Panneau de preferences pour Dimduc.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:08 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducPreferencesPanel extends BuAbstractPreferencesPanel {
  Hashtable optionsStr_;
  BuGridLayout loDimduc_;
  JPanel pnDimducCalq_;
  AbstractBorder boDimducCalq_;
  BuGridLayout loDimducCalq_;
  JLabel lbDimducCalqInter_;
  JComboBox liDimducCalqInter_;
  DimducPreferences options_;
  DimducImplementation dimduc_;
  public String getTitle() {
    return "Dimduc";
  }
  // Constructeur
  public DimducPreferencesPanel(final DimducImplementation _dimduc) {
    super();
    options_= DimducPreferences.DIMDUC;
    dimduc_= _dimduc;
    int nDimduc, nDimducCalq;
    Object items[];
    optionsStr_= new Hashtable();
    String item;
    String itemKey;
    optionsStr_.put(itemKey= "Orthogonales", item= "O-Interaction");
    optionsStr_.put(item, itemKey);
    optionsStr_.put(itemKey= "Dessin", item= "D-Interaction");
    optionsStr_.put(item, itemKey);
    // Panneau Dimduc
    loDimduc_= new BuGridLayout();
    loDimduc_.setColumns(1);
    this.setLayout(loDimduc_);
    nDimduc= 0;
    // Calques
    pnDimducCalq_= new JPanel();
    boDimducCalq_=
      new CompoundBorder(
        new TitledBorder("Calques"),
        new EmptyBorder(5, 5, 5, 5));
    pnDimducCalq_.setBorder(boDimducCalq_);
    loDimducCalq_= new BuGridLayout();
    loDimducCalq_.setColumns(2);
    loDimducCalq_.setVgap(5);
    loDimducCalq_.setHgap(5);
    pnDimducCalq_.setLayout(loDimducCalq_);
    nDimducCalq= 0;
    // Calque interactif
    lbDimducCalqInter_= new JLabel("Interactif");
    items= new String[2];
    items[0]= "Orthogonales";
    items[1]= "Dessin";
    liDimducCalqInter_= new JComboBox(items);
    liDimducCalqInter_.setEditable(false);
    pnDimducCalq_.add(lbDimducCalqInter_, nDimducCalq++);
    pnDimducCalq_.add(liDimducCalqInter_, nDimducCalq++);
    this.add(pnDimducCalq_, nDimduc++);
    updateComponents();
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(dimduc_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {
    final String interStr=
      (String)optionsStr_.get(liDimducCalqInter_.getSelectedItem());
    options_.putStringProperty(
      "dimduc.calques.interactif",
      interStr == null ? "" : interStr);
  }
  private void updateComponents() {
    liDimducCalqInter_.setSelectedItem(
      optionsStr_.get(options_.getStringProperty("dimduc.calques.interactif")));
  }
}
