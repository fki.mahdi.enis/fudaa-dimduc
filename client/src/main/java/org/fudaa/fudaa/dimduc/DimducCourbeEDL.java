/*
 * @file         DimducCourbeEDL.java
 * @creation     2000-06-08
 * @modification $Date: 2006-09-19 15:02:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.Locale;

import org.fudaa.dodico.corba.dimduc.SSollicitationAccostage;
import org.fudaa.dodico.corba.ducalbe.SPointEffortDeformation;

import org.fudaa.ebli.graphe.BGraphe;
/**
 * courbe ED defense duc le plus leger
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:08 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducCourbeEDL
  extends BGraphe
  implements PropertyChangeListener {
  private NumberFormat nf0_, nf2_;
  private SSollicitationAccostage[] sa_;
  private String GRA_TITRE;
  private String GRA_ABS_LABEL;
  private String GRA_ORD_LABEL;
  private String GRA_ABS_UNITE;
  private String GRA_ORD_UNITE;
  public DimducCourbeEDL(
    final SSollicitationAccostage[] _sa,
    final String gra_titre,
    final String gra_abs_label,
    final String gra_ord_label,
    final String gra_abs_unite,
    final String gra_ord_unite) {
    super();
    GRA_TITRE= gra_titre;
    GRA_ABS_LABEL= gra_abs_label;
    GRA_ORD_LABEL= gra_ord_label;
    GRA_ABS_UNITE= gra_abs_unite;
    GRA_ORD_UNITE= gra_ord_unite;
    nf0_= NumberFormat.getInstance(Locale.US);
    nf0_.setMaximumFractionDigits(0);
    nf0_.setGroupingUsed(false);
    nf2_= NumberFormat.getInstance(Locale.US);
    nf2_.setMaximumFractionDigits(3);
    nf2_.setGroupingUsed(false);
    sa_= _sa;
    setPreferredSize(new Dimension(640, 480));
    init();
  }
  private void init() {
    if (sa_ == null) {
      return;
    }
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      } // fin if
    } // fin try
    catch (final IOException ex) {
      System.err.println(ex);
    }
  } // fin void init ()
  public void setCourbeED(final SSollicitationAccostage[] s) {
    if (s == sa_) {
      return;
    }
    final SSollicitationAccostage[] vp= sa_;
    sa_= s;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("courbe_ED", vp, sa_);
  } // fin void setCourbeED
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("courbe_ED".equals(e.getPropertyName())) {
      final SSollicitationAccostage[] nv= (SSollicitationAccostage[])e.getNewValue();
      setCourbeED(nv);
      init();
    }
  }
  private InputStream generateGDF() throws IOException {
    if ((sa_ == null)) {
      return null;
    }
    // entree :
    final SPointEffortDeformation[] pedE_=
      sa_[0].ducAlbe.defense.courbesED[0].pointsED;
    final int nbPoint_E= pedE_.length; // entr�e
    System.out.println(" nombre de points entr�e = " + nbPoint_E);
    if (nbPoint_E <= 0) {
      return null;
    }
    String gdfStr= ""; // entr�e
    final String crbTitre= "Courbe ED DUC LE PLUS LEGER";
    String crbStrE= "";
    //calage de la courbe avec recherche des minis et des maxis :
    // initialisation :  si pas de point ? :
    double minDef= 0.0, maxDef= 80.0;
    double minEff= 0.0, maxEff= 0.0;
    // initialisation avec pt 0 :
    if (nbPoint_E > 0) {
      // minDef = maxDef = (pedE_[0].deformationDefense);
      maxEff= (pedE_[0].effortDefense);
    } // fin if(nbPoint_E >0)
    // recherche sur l'ensemble des valeurs :
    for (int i= 0; i < nbPoint_E; i++) {
      if (minDef > pedE_[i].deformationDefense) {
        minDef= pedE_[i].deformationDefense;
      }
      if (maxDef < pedE_[i].deformationDefense) {
        maxDef= pedE_[i].deformationDefense;
      }
      if (minEff > pedE_[i].effortDefense) {
        minEff= pedE_[i].effortDefense;
      }
      if (maxEff < pedE_[i].effortDefense) {
        maxEff= pedE_[i].effortDefense;
      }
      // alimentation chaine support graphique par valeurs points de la courbe
      crbStrE += "      "
        + nf0_.format(pedE_[i].deformationDefense)
        + " "
        + nf0_.format(pedE_[i].effortDefense)
        + "\n";
    } // fin boucle for
    // sortie
    final SPointEffortDeformation[] pedS_=
      sa_[0].ducAlbe.defense.courbesED[1].pointsED;
    final int nbPoint_S= pedS_.length; // sortie
    System.out.println(" nombre de points sortie = " + nbPoint_S);
    if (nbPoint_S <= 0) {
      return null;
    }
    String crbStrS= ""; // sortie
    // recherche sur l'ensemble des valeurs :  */
    for (int i= 0; i < nbPoint_S; i++) {
      // alimentation chaine support graphique par valeurs points de la courbe :
      crbStrS += "      "
        + nf0_.format(pedS_[i].deformationDefense)
        + " "
        + nf0_.format(pedS_[i].effortDefense)
        + "\n";
    } // fin boucle for
    // le graphe :
    gdfStr += "graphe\n{\n";
    gdfStr += "  titre \"" + crbTitre + "\n";
    gdfStr += "  animation non\n";
    gdfStr += "  legende oui\n";
    gdfStr += "  marges\n  {\n";
    gdfStr += "    gauche 60\n"; // 60
    gdfStr += "    droite 60\n"; // 60
    gdfStr += "    haut   45\n"; // 45
    gdfStr += "    bas    30\n  }\n"; // 30
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"" + GRA_ABS_LABEL + "\"\n";
    gdfStr += "    unite \"" + GRA_ABS_UNITE + "\"\n";
    gdfStr += "    orientation horizontal\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf2_.format(minDef) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxDef) + "\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"" + GRA_ORD_LABEL + "\"\n";
    gdfStr += "    unite \"" + GRA_ORD_UNITE + "\"\n";
    gdfStr += "    orientation vertical\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf0_.format(minEff) + "\n";
    gdfStr += "    maximum " + nf0_.format(maxEff) + "\n  }\n";
    // entree :
    gdfStr += "  courbe\n  {\n";
    gdfStr += "    titre \"" + GRA_TITRE + "\"\n";
    gdfStr += "    marqueurs oui\n";
    gdfStr += "    aspect\n    {\n";
    gdfStr += "    contour.couleur 00FFFF\n"; // bleu
    gdfStr += "    texte.couleur   00FFFF\n    }\n";
    gdfStr += "    valeurs\n    {\n";
    gdfStr += crbStrE;
    gdfStr += "    }\n  }\n";
    // sortie
    gdfStr += "  courbe\n  {\n";
    gdfStr += "    titre \"" + "\"\n";
    gdfStr += "    marqueurs oui\n";
    gdfStr += "    aspect\n    {\n";
    gdfStr += "    contour.couleur FF0000\n"; // rouge
    gdfStr += "    texte.couleur   FF0000\n    }\n";
    gdfStr += "    valeurs\n    {\n";
    gdfStr += crbStrS;
    gdfStr += "    }\n  }\n";
    return new ByteArrayInputStream(gdfStr.getBytes());
  }
} // fin class DimducCourbeEDL
