/*
 * @file         DimducAssistant.java
 * @creation     2001-01-21
 * @modification $Date: 2003-11-25 10:13:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import com.memoire.bu.BuAssistant;
/**
 * L'assistant du client Dimduc. Sous-classe de BuAssistant. Pas de surcharges.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:13:42 $ by $Author: deniger $
 * @author       Christian Barou 
 */
public class DimducAssistant extends BuAssistant {}
