/*
 * @creation     1999-06-01
 * @modification $Date: 2007-03-27 16:09:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dimduc;

import org.fudaa.dodico.corba.dimduc.IParametresDimduc;
import org.fudaa.dodico.corba.dimduc.IParametresDimducOperations;
import org.fudaa.dodico.corba.dimduc.SParametres01;
import org.fudaa.dodico.corba.dimduc.SParametres02;

import org.fudaa.dodico.calcul.DParametres;

/**
 * Classe d'implantation de l'interface IParametresDimduc gerant les parametres du code de calcul <code>dimduc</code>.
 *
 * @version $Revision: 1.10 $ $Date: 2007-03-27 16:09:59 $ by $Author: deniger $
 * @author Christian Barou
 */
public class DParametresDimduc extends DParametres implements IParametresDimduc, IParametresDimducOperations {
  /**
   * Structure decrivant les parametres : diametres, epaisseurs... cf "dimduc.idl".
   */
  private SParametres01 params01_;
  /**
   * Structure decrivant les parametres : contraintes admissibles,accostage... cf "dimduc.idl".
   */
  private SParametres02 params02_;

  /**
   * Constructeur vide.
   */
  public DParametresDimduc() {
    super();
  }

  /**
   * Description de la classe.
   *
   * @return "DParametresDimduc()"
   */
  public String toString() {
    return "DParametresDimduc()";
  }

  /**
   * Renvoie le structure interne utilisee pour decrire les diametres, les epaisseurs...cf "dimduc.idl".
   */
  public SParametres01 parametres01() {
    if (params01_ == null) {
      params01_ = new SParametres01();
    }
    return params01_;
  }

  /**
   * Modifie la structure interne correspondante.
   *
   * @param _p la nouvelle structure <code>SParametres01</code>
   */
  public void parametres01(final SParametres01 _p) {
    params01_ = _p;
  }

  /**
   * Renvoie la structure interne utilisee pour decrire les accostages, les contraintes admissibles, ....
   */
  public SParametres02 parametres02() {
    if (params02_ == null) {
      params02_ = new SParametres02();
    }
    return params02_;
  }

  /**
   * Modifie la structure interne correspondante.
   *
   * @param _p la nouvelle structure <code>SParametres02</code>
   */
  public void parametres02(final SParametres02 _p) {
    params02_ = _p;
  }
}
