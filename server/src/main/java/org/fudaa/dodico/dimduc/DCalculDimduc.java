/*
 * @creation 1999-06-01
 * @modification $Date: 2007-03-27 16:09:59 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.dimduc;

import org.fudaa.dodico.corba.dimduc.*;
import org.fudaa.dodico.corba.ducalbe.*;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe d'implantation de l'interface <code>ICalculDimduc</code> gerant l'execution du calcul <code>dimduc</code>.
 * Il n'y pas de de code de calcul dimduc: le code a ete reecrit en java.
 * 
 * @version $Revision: 1.13 $ $Date: 2007-03-27 16:09:59 $ by $Author: deniger $
 * @author Christian Barou
 */
public class DCalculDimduc extends DCalcul implements ICalculDimduc, ICalculDimducOperations {

  /**
   * ....
   */
  double epaisseur_cor_mm_;

  /**
   * Pour calcul force maxi et fiche :init profondeurs.
   */
  public byte prof_init_force_m_ = -20;

  /**
   * Pour calcul force maxi et fiche :init profondeurs.
   */
  public byte prof_init_fiche_m_ = -50;

  /**
   * ecran 1 : tete.
   */
  public double cote_tete_m_;

  /**
   * ecran 1 : tete.
   */
  public double fleche_maxi_tete_m_;

  /**
   * ecran 2 : tubes.
   */
  public double young_Mt_m2_;

  /**
   * ecran 2 : tubes.
   */
  public double entre_axe_m_;

  /**
   * ecran 2 : tubes.
   */
  public int nb_tubes_;

  /**
   * ecran 2 : tubes.
   */
  public int nb_sigma_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double diametre_min_mm_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double diametre_max_mm_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double diametre_pas_mm_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double epaisseur_min_mm_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double epaisseur_max_mm_;

  /**
   * Param pour iteration epaisseurs et diametres.
   */
  public double epaisseur_pas_mm_;

  /**
   * ....
   */
  public int[] contraintes_adm_t_m2 = new int[8];

  /**
   * ecran 3 : sols.
   */
  public int nb_sols_;

  /**
   * ....
   */
  public double[] cotes_sup_m_ = new double[2];

  /**
   * ....
   */
  public double[] cohesions_t_m2_ = new double[2];

  /**
   * ....
   */
  public double[] poids_sol_t_m3_ = new double[2];

  /**
   * Sans unite.
   */
  public double[] coeffs_butee_ = new double[2];

  /**
   * ecran 4 : accostages.
   */
  public int nb_acc_;

  /**
   * ....
   */
  public double[] energies_acc_tm_ = new double[10];

  /**
   * ....
   */
  public double[] cotes_acc_m_ = new double[10];

  /**
   * ....
   */
  public double[] reactions_acc_t_ = new double[10];

  /**
   * ecran 5 : donnees defense.
   */
  public double hauteur_defense_m_;

  /**
   * ecran 5 : donnees defense.
   */
  public double cote_mini_IV_m_;

  /**
   * ecran 5 : donnees defense.
   */
  public double cote_maxi_IV_m_;

  /**
   * A completer.
   */
  public int nb_courbes_ED_;

  /**
   * A completer.
   */
  public int nb_pointsED_E_;

  /**
   * 0 : abs. : d�formation en %, 1 : ord. : effort en t
   */
  public double[][] pointsED_E_ = new double[8][2];

  /**
   * boucles for : contraintes, accostages, sols, nb pts ED.
   */
  // public byte isigma;
  /**
   * boucles for : contraintes, accostages, sols, nb pts ED.
   */
  public byte iacc;

  /**
   * boucles for : contraintes, accostages, sols, nb pts ED.
   */
  // public byte isol;
  /**
   * boucles for : contraintes, accostages, sols, nb pts ED.
   */
  // public byte ied;
  /**
   * contrainte courante.
   */
  public int contrainte_adm_t_m2_;

  /**
   * accostage courant.
   */
  public double energie_acc_tm_;

  /**
   * accostage courant.
   */
  public double cote_acc_m_;

  /**
   * accostage courant.
   */
  public double reaction_acc_t_;

  /**
   * diametre et epaisseur courants.
   */
  public double diametre_m_;

  /**
   * diametre et epaisseur courants.
   */
  public double epaisseur_act_m_;

  /**
   * dans deroule4boucles : nombre d'ouvrages retenus.
   */
  public int nb_ducs_;

  /**
   * dans deroule4boucles : nombre d'ouvrages retenus.
   */
  public int nb_ducs_sigma_;

  /**
   * dans deroule4boucles : nombre d'ouvrages retenus.
   */
  public int nb_ducs_sigma_acc_;

  /**
   * n�s accostages.
   */
  public int no_acc_poids_min_acc_;

  /**
   * n�s accostages.
   */
  public int no_acc_pied_min_acc_;

  /**
   * dans calculeInertie.
   */
  public double inertie_m4_;

  /**
   * dans calculeMomentMaxiAdm.
   */
  public double moment_maxi_adm_tm_;

  /**
   * dans calculeForceMaxi.
   */
  public double force_maxi_t_; // moment_maxi_tm_;

  /**
   * ....
   */
  public boolean convergence_moment_;

  /**
   * dans calculeCoteFiche.
   */
  public double cote_fiche_m_;

  /**
   * dans calculeCoteFiche.
   */
  public double long_duc_m_;

  /**
   * ....
   */
  public boolean convergence_fiche_;

  /**
   * dans calculeCoteMomentMaxi.
   */
  public boolean convergence_cote_;

  /**
   * ....
   */
  public double moment_maxi_tm_;

  /**
   * ....
   */
  public double cote_moment_maxi_m_;

  /**
   * dans calculePoidsDuc.
   */
  public double poids_duc_t_;

  /**
   * dans calculeFleches.
   */
  public double fleche_choc_m_;

  /**
   * dans calculeFleches.
   */
  public double fleche_tete_m_;

  /**
   * dans calculeEnergieTubes.
   */
  public double energie_tubes_tm_;

  /**
   * dans calculeEnergieDefense.
   */
  public boolean iv_ ;

  public int nb_pointsED_S_;

  public double[][] pointsED_S_ = new double[8][2];

  public double deflexion_pc_;

  public double energie_defense_tm_;

  /**
   * dans calculeMomentFlechissant.
   */
  public double butee_Xm_cmf_t_;

  /**
   * dans calculeMomentFlechissant.
   */
  public double moment_sol_tm_;

  /**
   * dans calculeMomentFlechissant.
   */
  public double moment_Xm_tm_;

  /**
   * dans calculeMoment1Sol et calculeMoment2Sols.
   */
  public double moment_1sol_tm_;

  /**
   * dans calculeMoment1Sol et calculeMoment2Sols.
   */
  public double moment_2sols_tm_;

  /**
   * dans calculeButee1Sol et calculeButee2Sols.
   */
  private double butee_1sol_t_, butee_2sols_t_;

  /**
   * dans calculeDeriveeButee1Sol et calculeDeriveeButee2Sols.
   */
  private double derivee_butee_1sol_t_m_, derivee_butee_2sols_t_m_;

  /**
   * dans calculeDuc.
   */
  private double fiche_m_, fiche12_m_, cote_pied_m_, energie_totale_tm_;

  /**
   * dans valideDuc.
   */
  public boolean convient_duc_;

  /**
   * dans verifieDucPlusLegerSigma.
   */
  private double poids_min_sigma_t_, dia_poids_min_sigma_m_, epais_poids_min_sigma_m_;

  /**
   * dans verifieDucPlusLegerSigmaAcc.
   */
  private double poids_min_sigma_acc_t_, dia_poids_min_sigma_acc_m_, epais_poids_min_sigma_acc_m_;

  /**
   * dans verifieDucPlusCourtSigma.
   */
  private double cote_pied_min_sigma_m_, dia_pied_min_sigma_m_, epais_pied_min_sigma_m_;

  /**
   * dans verifieDucPlusCourtSigmaAcc.
   */
  private double cote_pied_min_sigma_acc_m_, dia_pied_min_sigma_acc_m_, epais_pied_min_sigma_acc_m_;

  /**
   * dans editeTableauMomentsLegerSigmaAcc.
   */
  public double[] cotes_moments_leger_m_ = new double[100];

  /**
   * ....
   */
  public double[] moments_leger_tm_ = new double[100];

  /**
   * dans editeTableauMomentsCourtSigmaAcc.
   */
  public double[] cotes_moments_court_m_ = new double[100];

  /**
   * ....
   */
  public double[] moments_court_tm_ = new double[100];

  /**
   * Pour calcul poids ouvrage.
   */
  public final static double POIDS_ACIER_T_M3 = 7.85;

  /**
   * Precision convergence.
   */
  public final static double PREC_PROF_M = 0.01;

  /**
   * Precision convergence.
   */
  public final static double PREC_ENERGIE_TM = 0.01;

  /**
   * nombre iterations profondeurs.
   */
  public final static byte NB_ITER = 100;

  /**
   * Initialisation des extensions de fichier utilisees : "dimduc_in,dimduc_out".
   */
  public DCalculDimduc() {
    super();
    setFichiersExtensions(new String[] { ".dimduc_in", ".dimduc_out" });
  }

  /**
   * Description de la classe.
   * 
   * @return "DCalculDimduc()"
   */
  public String toString() {
    return "DCalculDimduc()";
  }

  /**
   * Description du serveur de calcul.
   */
  public String description() {
    return "dimduc, serveur de calcul " + super.description();
  }

  /**
   * Apres verification de la connexion et des interfaces de parametres et de resultats, lance le calcul ( code java)
   * puis complete l'interface de resultats.
   * 
   * @param _c la connexion utilisee.
   */
  public void calcul(final IConnexion _c) {
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresDimduc params = IParametresDimducHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    final IResultatsDimduc results = IResultatsDimducHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    results.initialiseResultats();
    log(_c, "lancement du calcul");
    try {
      // recuperation parametres saisis : IParametres, CParametres
      cote_tete_m_ = params.parametres01().coteTete;
      fleche_maxi_tete_m_ = params.parametres01().flecheMaxiTete;
      // nombres accostages, contraintes, couches de sol (boucles)
      nb_acc_ = params.parametres01().nbAccostages + 1;
      nb_sigma_ = params.parametres01().nbSigma + 1;
      nb_sols_ = params.parametres01().nbCouches + 1;
      // diametres et epaisseurs pour iteration :
      diametre_min_mm_ = params.parametres01().diametreMini;
      diametre_max_mm_ = params.parametres01().diametreMaxi;
      diametre_pas_mm_ = params.parametres01().diametrePas;
      epaisseur_min_mm_ = params.parametres01().epaisseurMini;
      epaisseur_max_mm_ = params.parametres01().epaisseurMaxi;
      epaisseur_pas_mm_ = params.parametres01().epaisseurPas;
      epaisseur_cor_mm_ = params.parametres01().epaisseurCor;
      // contraintes :
      for (int isigma = 0; isigma < nb_sigma_; isigma++) {
        contraintes_adm_t_m2[isigma] = params.parametres02().contraintesAdm[isigma].contrainteAdm;
      }
      // couches de sol :
      for (int isol = 0; isol < nb_sols_; isol++) {
        cotes_sup_m_[isol] = params.parametres02().couchesSol[isol].coteSuperieure;
        cohesions_t_m2_[isol] = params.parametres02().couchesSol[isol].cohesionMini;
        coeffs_butee_[isol] = params.parametres02().couchesSol[isol].buteeMini;
        poids_sol_t_m3_[isol] = params.parametres02().couchesSol[isol].poidsMini;
      }
      // accostages :
      for (iacc = 0; iacc < 10; iacc++) {
        energies_acc_tm_[iacc] = params.parametres02().accostages[iacc].energieAccostage;
        cotes_acc_m_[iacc] = params.parametres02().accostages[iacc].coteAccostage;
        reactions_acc_t_[iacc] = params.parametres02().accostages[iacc].reactionAdmissible;
      }
      // divers
      nb_tubes_ = params.parametres02().ensembleTubes.nombreTubes + 1;
      entre_axe_m_ = params.parametres02().ensembleTubes.entreAxe;
      young_Mt_m2_ = params.parametres02().ensembleTubes.moduleYoungTubes;
      // defense
      hauteur_defense_m_ = params.parametres02().defense.hauteurDefense * 0.001;
      // nombre de points de la courbe Effort Deformation :
      nb_pointsED_E_ = params.parametres01().nbPointsED;
      nb_courbes_ED_ = params.parametres02().defense.nombreCourbesED;
      // courbe ED :
      cote_mini_IV_m_ = params.parametres02().defense.courbesED[0].coteMini;
      cote_maxi_IV_m_ = params.parametres02().defense.courbesED[0].coteMaxi;
      if (cote_maxi_IV_m_ - cote_mini_IV_m_ > 0.2) {
        iv_ = true;
      }
      // points ED
      for (int ied = 0; ied < nb_pointsED_E_; ied++) {
        pointsED_E_[ied][0] = params.parametres02().defense.courbesED[0].pointsED[ied].deformationDefense;
        pointsED_E_[ied][1] = params.parametres02().defense.courbesED[0].pointsED[ied].effortDefense;
      }
      // lancement rappel donnees et calcul ouvrage

      rappelleDonnees();
      final String err = deroule4Boucles();
      results.erreur(err);
      if (err != null) {
        return;
      }
      // SSollicitationAccostage :
      final SSollicitationAccostage[] sa = new SSollicitationAccostage[2];
      // Duc le + leger (poids_min_): caracteristiques (sa[0])et moments (tml)
      // initialisations aux valeurs duc + leger puis lancement calcul :
      diametre_m_ = dia_poids_min_sigma_m_;
      epaisseur_act_m_ = epais_poids_min_sigma_m_;
      // appel module de calcul principal :
      calculeDuc();
      // instanciations SpointEffortDeformation :
      final SPointEffortDeformation[] pedl = new SPointEffortDeformation[nb_pointsED_E_ + nb_pointsED_S_];
      // recuperation pts saisis en entree, caracteristiques de la courbe ED de
      // la defense :
      for (int ied = 0; ied < nb_pointsED_E_; ied++) {
        pedl[ied] = new SPointEffortDeformation();
        pedl[ied].deformationDefense = pointsED_E_[ied][0];
        pedl[ied].effortDefense = pointsED_E_[ied][1];
      } // fin boucle for ied
      /*
       * recuperation pts interceptes dans le calcul de l'energie de la defense, caracteristiques de l'exemple en cours =
       * pts precedents jusqu'a ED exemple, + ED exemple + pt 0,0
       */
      for (int ied = 0; ied < nb_pointsED_S_; ied++) {
        pedl[ied] = new SPointEffortDeformation();
        pedl[ied].deformationDefense = pointsED_S_[ied][0];
        pedl[ied].effortDefense = pointsED_S_[ied][1];
      }
      // instanciations SollicitationsAccostage : SA [0] : + leger
      sa[0] = new SSollicitationAccostage();
      // Saccostage
      sa[0].accostage = new SAccostage();
      sa[0].accostage.coteAccostage = cote_acc_m_;
      sa[0].accostage.energieAccostage = energie_acc_tm_;
      sa[0].accostage.reactionAdmissible = reaction_acc_t_;
      // SDucAlbeDimduc
      sa[0].ducAlbe = new SDucAlbeDimduc();
      // SDefense
      sa[0].ducAlbe.defense = new SDefense();
      sa[0].ducAlbe.defense.hauteurDefense = hauteur_defense_m_ * 1000; // en mm
      sa[0].ducAlbe.defense.nombreCourbesED = nb_courbes_ED_;
      sa[0].ducAlbe.defense.energieDefense = energie_defense_tm_;
      sa[0].ducAlbe.energieDucAlbe = energie_totale_tm_;
      sa[0].ducAlbe.defense.courbesED = new SCourbeEffortDeformation[2];
      // courbe 0 : entree
      sa[0].ducAlbe.defense.courbesED[0] = new SCourbeEffortDeformation();
      sa[0].ducAlbe.defense.courbesED[0].pointsED = new SPointEffortDeformation[nb_pointsED_E_];
      // entree
      /*
       * recuperation pts saisis en entree, caracteristiques de la courbe ED de la defense :
       */
      for (int ied = 0; ied < nb_pointsED_E_; ied++) {
        sa[0].ducAlbe.defense.courbesED[0].pointsED[ied] = new SPointEffortDeformation();
        sa[0].ducAlbe.defense.courbesED[0].pointsED[ied].deformationDefense = pointsED_E_[ied][0];
        sa[0].ducAlbe.defense.courbesED[0].pointsED[ied].effortDefense = pointsED_E_[ied][1];
      } // fin boucle for ied
      // courbe 1 : sortie
      sa[0].ducAlbe.defense.courbesED[1] = new SCourbeEffortDeformation();
      sa[0].ducAlbe.defense.courbesED[1].pointsED = new SPointEffortDeformation[nb_pointsED_S_];
      // sortie
      /*
       * recuperation pts interceptes dans le calcul de l'energie de la defense, caracteristiques de l'exemple en cours =
       * pts precedents jusqu'a ED exemple, + ED exemple + pt 0,0
       */
      for (int ied = 0; ied < nb_pointsED_S_; ied++) {
        sa[0].ducAlbe.defense.courbesED[1].pointsED[ied] = new SPointEffortDeformation();
        sa[0].ducAlbe.defense.courbesED[1].pointsED[ied].deformationDefense = pointsED_S_[ied][0];
        sa[0].ducAlbe.defense.courbesED[1].pointsED[ied].effortDefense = pointsED_S_[ied][1];
      } // fin boucle for ied
      // SDucAlbeSansDefense :
      sa[0].ducAlbe.ducAlbeSansDefense = new SDucAlbeSansDefense();
      sa[0].ducAlbe.ducAlbeSansDefense.coteCbutee = fiche12_m_; // C
      sa[0].ducAlbe.ducAlbeSansDefense.coteChoc = cote_acc_m_; // P
      sa[0].ducAlbe.ducAlbeSansDefense.coteTete = cote_tete_m_; // P
      sa[0].ducAlbe.ducAlbeSansDefense.cotePied = cote_pied_m_; // C
      sa[0].ducAlbe.ducAlbeSansDefense.momentMaxi = moment_maxi_tm_; // C
      sa[0].ducAlbe.ducAlbeSansDefense.coteMomentMaxi = cote_moment_maxi_m_;
      // C
      sa[0].ducAlbe.ducAlbeSansDefense.forceMaxi = force_maxi_t_; // C
      sa[0].ducAlbe.ducAlbeSansDefense.flecheEnTete = fleche_tete_m_; // C
      sa[0].ducAlbe.ducAlbeSansDefense.flecheAuChoc = fleche_choc_m_; // C
      // SEnsembleTubes
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes = new SEnsembleTubes();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.energieTubes = energie_tubes_tm_;
      // SContrainteAdmissible
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes = new SContrainteAdmissible();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes.contrainteAdm = contrainte_adm_t_m2_;
      // STube
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes = new STube[10];
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0] = new STube();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube = new STroncon[10];
      // STroncon SDiametre SEpaisseur
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0] = new STroncon();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon = new SDiametreTroncon();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon = new SEpaisseurTroncon();
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].poidsTroncon = poids_duc_t_;
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon.diametre = dia_poids_min_sigma_m_ * 1000;
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurCorrodee = epaisseur_cor_mm_;
      sa[0].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon.epaisseur = epais_poids_min_sigma_m_ * 1000;
      // fin sa 0 duc le plus leger
      /*
       * instanciations tml : tableau moments duc + leger, entre cote accostage et cote pied :
       */
      final STableauMoments[] tml = new STableauMoments[(int) (cote_acc_m_ - cote_fiche_m_ + 2)];
      for (int iml = 0; iml <= (int) (cote_acc_m_ - cote_fiche_m_ + 1); iml++) {
        tml[iml] = new STableauMoments();
        tml[iml].coteMoment = cotes_moments_leger_m_[iml];
        tml[iml].valeurMoment = moments_leger_tm_[iml];
      } // fin boucle for
      // fin Tableau moment plus leger
      /*
       * Duc le + court (pied_min_): caracteristiques (sa[1])et moments (tmc) initialisation aux valeurs + court et
       * lancement calcul
       */
      diametre_m_ = dia_pied_min_sigma_m_;
      epaisseur_act_m_ = epais_pied_min_sigma_m_;
      // appel module de calcul principal :
      calculeDuc();
      // instanciations SpointEffortDeformation + court:
      final SPointEffortDeformation[] pedc = new SPointEffortDeformation[nb_pointsED_E_ + nb_pointsED_S_];
      /*
       * recuperation pts saisis en entree, caracteristiques de la courbe ED de la defense :
       */
      for (int ied = 0; ied < nb_pointsED_E_; ied++) {
        pedc[ied] = new SPointEffortDeformation();
        pedc[ied].deformationDefense = pointsED_E_[ied][0];
        pedc[ied].effortDefense = pointsED_E_[ied][1];
      } // fin boucle for ied
      /*
       * recuperation pts interceptes dans le calcul de l'energie de la defense, caracteristiques de l'exemple en cours =
       * pts precedents jusqu'a ED exemple, + ED exemple + pt 0,0
       */
      for (int ied = 0; ied < nb_pointsED_S_; ied++) {
        pedc[ied] = new SPointEffortDeformation();
        pedc[ied].deformationDefense = pointsED_S_[ied][0];
        pedc[ied].effortDefense = pointsED_S_[ied][1];
      }
      // instanciations SA[1] : + court :
      sa[1] = new SSollicitationAccostage();
      // SAccostage
      sa[1].accostage = new SAccostage();
      sa[1].accostage.coteAccostage = cote_acc_m_;
      sa[1].accostage.energieAccostage = energie_acc_tm_;
      sa[1].accostage.reactionAdmissible = reaction_acc_t_;
      // SDucAlbeDimduc
      sa[1].ducAlbe = new SDucAlbeDimduc();
      // SDefense
      sa[1].ducAlbe.defense = new SDefense();
      sa[1].ducAlbe.defense.hauteurDefense = hauteur_defense_m_ * 1000;
      sa[1].ducAlbe.defense.energieDefense = energie_defense_tm_;
      sa[1].ducAlbe.defense.nombreCourbesED = nb_courbes_ED_;
      sa[1].ducAlbe.energieDucAlbe = energie_totale_tm_;
      sa[1].ducAlbe.defense.courbesED = new SCourbeEffortDeformation[2];
      // courbe 0 : entree
      sa[1].ducAlbe.defense.courbesED[0] = new SCourbeEffortDeformation();
      sa[1].ducAlbe.defense.courbesED[0].pointsED = new SPointEffortDeformation[nb_pointsED_E_];
      // entree
      /*
       * recuperation pts saisis en entree, caracteristiques de la courbe ED de la defense
       */
      for (int ied = 0; ied < nb_pointsED_E_; ied++) {
        sa[1].ducAlbe.defense.courbesED[0].pointsED[ied] = new SPointEffortDeformation();
        sa[1].ducAlbe.defense.courbesED[0].pointsED[ied].deformationDefense = pointsED_E_[ied][0];
        sa[1].ducAlbe.defense.courbesED[0].pointsED[ied].effortDefense = pointsED_E_[ied][1];
      } // fin boucle for ied
      // courbe 1 : sortie
      sa[1].ducAlbe.defense.courbesED[1] = new SCourbeEffortDeformation();
      sa[1].ducAlbe.defense.courbesED[1].pointsED = new SPointEffortDeformation[nb_pointsED_S_];
      // sortie
      /*
       * recuperation pts interceptes dans le calcul de l'energie de la defense, caracteristiques de l'exemple en cours =
       * pts precedents jusqu'a ED exemple, + ED exemple + pt 0,0
       */
      for (int ied = 0; ied < nb_pointsED_S_; ied++) {
        sa[1].ducAlbe.defense.courbesED[1].pointsED[ied] = new SPointEffortDeformation();
        sa[1].ducAlbe.defense.courbesED[1].pointsED[ied].deformationDefense = pointsED_S_[ied][0];
        sa[1].ducAlbe.defense.courbesED[1].pointsED[ied].effortDefense = pointsED_S_[ied][1];
      } // fin boucle for ied
      // SDucAlbeSansDefense :
      sa[1].ducAlbe.ducAlbeSansDefense = new SDucAlbeSansDefense();
      sa[1].ducAlbe.ducAlbeSansDefense.coteCbutee = fiche12_m_; // C
      sa[1].ducAlbe.ducAlbeSansDefense.coteChoc = cote_acc_m_; // P
      sa[1].ducAlbe.ducAlbeSansDefense.coteTete = cote_tete_m_; // P
      sa[1].ducAlbe.ducAlbeSansDefense.cotePied = cote_pied_m_; // C
      sa[1].ducAlbe.ducAlbeSansDefense.momentMaxi = moment_maxi_tm_; // C
      sa[1].ducAlbe.ducAlbeSansDefense.coteMomentMaxi = cote_moment_maxi_m_;
      // C
      sa[1].ducAlbe.ducAlbeSansDefense.forceMaxi = force_maxi_t_; // C
      sa[1].ducAlbe.ducAlbeSansDefense.flecheEnTete = fleche_tete_m_; // C
      sa[1].ducAlbe.ducAlbeSansDefense.flecheAuChoc = fleche_choc_m_; // C
      // SEnsembleTubes
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes = new SEnsembleTubes();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.energieTubes = energie_tubes_tm_;
      // SContrainteAdmissible
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes = new SContrainteAdmissible();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.contrainteAdmTubes.contrainteAdm = contrainte_adm_t_m2_;
      // STube
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes = new STube[10];
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0] = new STube();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube = new STroncon[10];
      // STroncon Sdiametre SEpaisseur
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0] = new STroncon();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon = new SDiametreTroncon();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon = new SEpaisseurTroncon();
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].poidsTroncon = poids_duc_t_;
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].diametreTroncon.diametre = (dia_pied_min_sigma_m_ * 1000);
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurCorrodee = epaisseur_cor_mm_;
      sa[1].ducAlbe.ducAlbeSansDefense.ensembleTubes.tubes[0].tube[0].epaisseurTroncon.epaisseur = (epais_pied_min_sigma_m_ * 1000);
      // fin SA [1]
      // instanciation tmc : tableau moments duc + court, entre cote accostage
      // et cote pied :
      final STableauMoments[] tmc = new STableauMoments[(int) (cote_acc_m_ - cote_fiche_m_ + 2)];
      for (int imc = 0; imc <= (int) (cote_acc_m_ - cote_fiche_m_ + 1); imc++) {
        tmc[imc] = new STableauMoments();
        tmc[imc].coteMoment = cotes_moments_court_m_[imc];
        tmc[imc].valeurMoment = moments_court_tm_[imc];
      } // fin boucle for
      // fin Tableau moment plus court
      // constitution results avant passage resultats :
      if (results.erreur() == null) {
        results.sollicitationsAccostage(sa);
        results.pointsEDL(pedl);
        results.pointsEDC(pedc);
        results.tableauMomentsLeger(tml);
        results.tableauMomentsCourt(tmc);
      }
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }

  /**
   * Ecrit sur la sortie standart les donnees du calcul.
   */
  public void rappelleDonnees() {
    try {
      outNewLine();
      System.out.println(" - - - RAPPEL DONNEES - - -  :");
      outNewLine();
      System.out.println("cote tete ouvrage = " + cote_tete_m_ + " m; fleche maxi adm. en tete = "
          + fleche_maxi_tete_m_ + " m");
      System.out.println("nombre de tubes   =  " + nb_tubes_);
      System.out.println("distance entre axes tubes =  " + entre_axe_m_ + " m");
      System.out.println("module d'Young =  " + young_Mt_m2_ + "Megat/m2");
      outNewLine();
      System.out.println("Rappel donnees contraintes au nombre de :" + nb_sigma_);
      for (int isigma = 0; isigma < nb_sigma_; isigma++) {
        System.out.println("contrainte n� " + (isigma + 1) + " : " + contraintes_adm_t_m2[isigma] + " t/m2");
      } // fin boucle for isigma rappel contraintes
      outNewLine();
      System.out.println("Rappel donnees couches sol au nombre de :" + nb_sols_);
      for (int isol = 0; isol < nb_sols_; isol++) {
        System.out.println(" sol n� " + (isol + 1) + " : poids = " + poids_sol_t_m3_[isol] + " t/m3, cote sup " + " = "
            + cotes_sup_m_[isol] + " m; cohesion " + " = " + cohesions_t_m2_[isol] + " t/m2; butee " + " = "
            + coeffs_butee_[isol]);
      } // fin boucle for isol rappel donnees sol
      outNewLine();
      System.out.println("Rappel donnees accostages au nombre de : " + nb_acc_);
      for (iacc = 0; iacc < nb_acc_; iacc++) {
        System.out.println("accostage n� " + (iacc + 1) + " : energie = " + energies_acc_tm_[iacc] + " tm + cote = "
            + cotes_acc_m_[iacc] + " m + reaction maxi =  " + reactions_acc_t_[iacc] + " t");
      } // fin boucle for iacc rappel accostages
      outNewLine();
      /*
       * voir qd catalogue pour diam et epaisseurs System.out.println ("Rappel donnees diametres au nombre de : " +
       * nb_diametres_); for (byte idia = 0; idia < nb_diametres_; idia ++) {System.out.println ("diametre n� " + idia+1 + " : " +
       * diametres_[idia] + " mm"); } // fin boucle for idia rappel diametres System.out.println (" "); // fin diametre
       * catalogue
       */
      System.out.println("Rappel donnees diametres en mm  :");
      System.out.println("mini = " + diametre_min_mm_ + " . maxi = " + diametre_max_mm_ + " . pas = "
          + diametre_pas_mm_);
      outNewLine();
      System.out.println("Rappel donnees epaisseurs en mm  :");
      System.out.println("mini = " + epaisseur_min_mm_ + " . maxi = " + epaisseur_max_mm_ + " . pas = "
          + epaisseur_pas_mm_ + " . epaisseur corrodee = " + epaisseur_cor_mm_);
      outNewLine();
      if (nb_pointsED_E_ > 0) {
        System.out.println("Rappel donnees defense");
        System.out.println("hauteur = " + hauteur_defense_m_ + " m");
        System.out.println("courbe effort deformation de la defense :");
        System.out.println("Cote mini = " + cote_mini_IV_m_ + " m, cote maxi = " + cote_maxi_IV_m_);
        for (int ied = 0; ied < nb_pointsED_E_; ied++) {
          System.out.println("Effort " + (ied + 1) + " = " + pointsED_E_[ied][1] + " t. Deformation " + (ied + 1)
              + " = " + pointsED_E_[ied][0] + " %");
        } // fin boucle for ied
        outNewLine();
      } // fin if (nb_pointsED_E_> 0)
      System.out.println(" - - - FIN RAPPEL DONNEES - - - ");
    } // fin try
    catch (final Exception e) {}
  } // fin methode rappelledonnees

  private void outNewLine() {
    System.out.println(" ");
  }

  /**
   * Routine interne pour calculer: ???.
   */
  public String deroule4Boucles() {
    try {
      outNewLine();
      // init nombre d'ouvrages convenant :
      nb_ducs_ = 0;
      // boucle n� 1 sur les contraintes :
      for (int isigma = 0; isigma < nb_sigma_; isigma++) {
        // initialisations :
        contrainte_adm_t_m2_ = contraintes_adm_t_m2[isigma];
        // init nombre d'ouvrages convenant pour la contrainte sigma :
        nb_ducs_sigma_ = 0;
        // init valeurs fortes pour recherche duc + l�ger, + court pour la
        // contrainte sigma :
        poids_min_sigma_t_ = 100000;
        cote_pied_min_sigma_m_ = -100;
        // init nos accostages + court, + leger :
        no_acc_poids_min_acc_ = 0;
        no_acc_pied_min_acc_ = 0;
        // boucle n� 2 sur les accostages :
        for (iacc = 0; iacc < nb_acc_; iacc++) {
          // initialisations :
          // accostage :
          cote_acc_m_ = cotes_acc_m_[iacc];
          energie_acc_tm_ = energies_acc_tm_[iacc];
          reaction_acc_t_ = reactions_acc_t_[iacc];
          // nombre d'ouvrages convenant pour le doublet contrainte
          // sigma/accostage :
          nb_ducs_sigma_acc_ = 0;
          // init valeurs fortes pour recherche duc + l�ger, + court pour le
          // doublet contrainte
          // sigma/accostage :
          poids_min_sigma_acc_t_ = 100000;
          cote_pied_min_sigma_acc_m_ = -100;
          // out.println :
          System.out.println(" - BOUCLE CONTRAINTE N� " + (isigma + 1) + " = " + contrainte_adm_t_m2_
              + " t/m2; ACCOSTAGE N � " + (iacc + 1) + " : cote " + cote_acc_m_ + " m; energie " + energie_acc_tm_
              + " tm; r�action " + reaction_acc_t_ + " t");
          outNewLine();
          // boucle n� 3 : diametres en m, iteres . A suivre : dans tableau
          // (catalogue):
          for (int diametre_tot_mm_ = (int) diametre_min_mm_; diametre_tot_mm_ <= diametre_max_mm_; diametre_tot_mm_ += diametre_pas_mm_) {
            // initialisation et conversion en m :
            diametre_m_ = diametre_tot_mm_ * 0.001;
            // boucle n� 4 : epaisseurs iterees : act : active, tot : totale,
            // cor : corrodee
            for (int epaisseur_tot_mm_ = (int) epaisseur_min_mm_; epaisseur_tot_mm_ <= epaisseur_max_mm_; epaisseur_tot_mm_ += epaisseur_pas_mm_) {
              // enleve l'epaisseur corrodee qui sera rajoutee uniqt pour le
              // calcul du poids;
              // convertit en m :
              epaisseur_act_m_ = (epaisseur_tot_mm_ - epaisseur_cor_mm_) * 0.001;
              // appel modules de calcul principal et de validation :
              calculeDuc();
              // module de validation :
              valideDuc(fleche_tete_m_, force_maxi_t_, moment_Xm_tm_, convient_duc_);
              if (convient_duc_) {
                // incrementations nb_ducs_*:
                nb_ducs_ = nb_ducs_ + 1;
                nb_ducs_sigma_ = nb_ducs_sigma_ + 1;
                nb_ducs_sigma_acc_ = nb_ducs_sigma_acc_ + 1;
                System.out.println(" Edition donn�es Duc n� " + nb_ducs_sigma_acc_
                    + " convenant pour contrainte/accostage : ");
                editeDuc();
                outNewLine();
                // tests pour + leger, + court :
                verifieDucPlusLegerSigmaAcc();
                verifieDucPlusLegerSigma();
                verifieDucPlusCourtSigmaAcc();
                verifieDucPlusCourtAcc();
              } // fin if convient_duc_
            } // fin boucle 4 epaisseur_tot_mm_ epaisseurs
          } // fin boucle 3 diametre_tot_mm_diametres
          if (nb_ducs_sigma_acc_ > 0) {
            editeDucPlusLegerSigmaAcc(isigma);
            editeDucPlusCourtSigmaAcc(isigma);
            editeTableauMomentsLegerSigmaAcc(isigma);
            editeTableauMomentsCourtSigmaAcc(isigma);
          } else {
            System.out.println(" Aucun ouvrage ne convient pour contrainte n� " + (isigma + 1) + " = "
                + contrainte_adm_t_m2_ + " t/m2; accostage n� " + (iacc + 1));
          } // fin else
          outNewLine();
          System.out.println(" - FIN BOUCLE CONTRAINTE N� " + (isigma + 1) + " = " + contrainte_adm_t_m2_
              + " t/m2; ACCOSTAGE N � " + (iacc + 1) + " : cote " + cote_acc_m_ + " m; energie " + energie_acc_tm_
              + " tm; reaction " + reaction_acc_t_ + " t ");
          outNewLine();
        } // fin boucle 2 for iacc accostages
        if (nb_ducs_sigma_ > 0) {
          editeDucPlusLegerSigma(isigma);
          editeDucPlusCourtSigma(isigma);
        } else {
          System.out.println(" Aucun ouvrage ne convient pour contrainte n� " + contrainte_adm_t_m2_ + " t/m2");

        } // fin else
        outNewLine();
        System.out.println(" - FIN BOUCLE CONTRAINTE N� " + (isigma + 1) + " = " + contrainte_adm_t_m2_ + " t/m2");
        outNewLine();
      } // fin boucle 1 for isigma contraintes
      // fin de l'execution du code
      System.out.println(" - FIN 4 BOUCLES - ");

    } // fin try
    catch (final Exception e) {}
    if (nb_ducs_ <= 0) {
      return "Aucun ouvrage ne convient";
    } // fin else
    // tout est ok
    return null;
  } // fin methode derouleQuatreBoucles

  /**
   * Suivant les parametres donnes, verifie la validite du duc.
   * 
   * @param _rFlecheTeteVdM
   * @param _rForceMaxiVdT
   * @param _rMomentVdTm
   * @param _eConvientDucVd
   */
  public void valideDuc(final double _rFlecheTeteVdM, final double _rForceMaxiVdT, final double _rMomentVdTm,
      final boolean _eConvientDucVd) {
    // initialisations :
    boolean eConvientDucVd = true;
    // test n�1 sur fleche en tete :
    if (fleche_maxi_tete_m_ > 0) {
      if (_rFlecheTeteVdM > fleche_maxi_tete_m_) {
        /*
         * System.out.println ("NON : FLECHE EN TETE TROP IMPORTANTE "); System.out.println ("FLECHE EN TETE = " +
         * _r_fleche_tete_vd_m + " M. > " + fleche_maxi_tete_m_+ " M");
         */
        eConvientDucVd = false;
      } // fin if (_r_fleche_tete_vd_m > fleche_maxi_tete_m_)
    } // fin if (fleche_maxi_tete_m_> 0)
    // test n� 2 sur reaction accostage :
    if (reaction_acc_t_ > 0) {
      if (_rForceMaxiVdT > reaction_acc_t_) {
        /*
         * System.out.println ("NON : REACTION TROP IMPORTANTE "); System.out.println ("REACTION = " +
         * _r_force_maxi_vd_t + " T > " + reaction_acc_t_+ " T");
         */
        eConvientDucVd = false;
      } // fin if (_r_force_maxi_vd_t > reaction_acc_t_)
    } // fin if (reaction_acc_t_ > 0)
    // test n� 3 sur moment maxi :
    if (_rMomentVdTm > moment_maxi_adm_tm_) {
      /*
       * System.out.println ("NON : MOMENT TROP IMPORTANT :"); System.out.println ("MOMENT = " + _r_moment_vd_tm + " TM >
       * MOMENT MAXI ADM. : " + moment_maxi_adm_tm_+ " TM");
       */
      eConvientDucVd = false;
    } // fin if (_r_moment_vd_tm > moment_maxi_adm_tm_)
    // affectation variable globale convient_duc_ :
    convient_duc_ = eConvientDucVd;
  } // fin methode valideDuc v 1.05

  /**
   * Renvoie sur la sortie standart des informations concernant le duv.
   */
  public void editeDuc() {
    try {
      outNewLine();
      System.out.println("editeDuc : poids ouvrage = " + poids_duc_t_ + " t; diametre = " + diametre_m_
          + " m; �paisseur  = " + epaisseur_act_m_ + " m");
      System.out.println("moment maxi = " + moment_maxi_tm_ + " tm a la cote " + cote_moment_maxi_m_
          + " m avec force maxi applic. = " + force_maxi_t_ + " t");
      System.out.println("fiche corrigee = " + fiche12_m_ + " m; cote pied = " + cote_pied_m_ + " m ");
      System.out.println("fleche choc = " + fleche_choc_m_ + " m a la cote " + cote_acc_m_ + " m; fl�che en tete "
          + fleche_tete_m_ + " m a la cote " + cote_tete_m_ + " m");
      System.out.println("energie absorbee par tubes = " + energie_tubes_tm_ + " tm");
      if (nb_pointsED_E_ > 0) {
        // existence defense
        System.out.println("energie absorbee par defense = " + energie_defense_tm_ + " tm avec deflexion = "
            + deflexion_pc_ + "%");
      }
      System.out.println("energie totale absorbee = " + energie_totale_tm_ + " tm");
      outNewLine();
    } // fin try
    catch (final Exception e) {}
  } // fin methode editeDuc

  /**
   * Methode calcul du Duc d'Albe le + leger pour un doublet contrainte/accostage.
   */
  public void verifieDucPlusLegerSigmaAcc() {
    if (poids_duc_t_ < poids_min_sigma_acc_t_) {
      // + leger
      // enregistrement caract. + leger :
      poids_min_sigma_acc_t_ = poids_duc_t_;
      dia_poids_min_sigma_acc_m_ = diametre_m_;
      epais_poids_min_sigma_acc_m_ = epaisseur_act_m_;
      /*
       * debuggage : System.out.println ("Void verifieDucPlusLegerSigmaAcc : Nouveau duc + leger : poids = " +
       * poids_min_sigma_acc_t_ + " t, diametre = " + dia_poids_min_sigma_acc_m_+ "m, epaisseur = " +
       * epais_poids_min_sigma_acc_m_ + " m");
       */
    } // fin if (poids_duc_t_< poids_min_sigma_acc_t_)
  } // fin methode verifieDucPlusLegerSigmaAcc

  /**
   * Methode calcul Duc Albe le + leger pour une contrainte, tous les accostages.
   */
  public void verifieDucPlusLegerSigma() {
    if (poids_min_sigma_acc_t_ < poids_min_sigma_t_) {
      // + leger
      // enregistrement caract. + leger :
      poids_min_sigma_t_ = poids_min_sigma_acc_t_;
      dia_poids_min_sigma_m_ = dia_poids_min_sigma_acc_m_;
      epais_poids_min_sigma_m_ = epais_poids_min_sigma_acc_m_;
      // enregistrement du numero accostage duc le + leger :
      no_acc_poids_min_acc_ = iacc;
      /*
       * debuggage : System.out.println ("Void verifieDucPlusLegerSigma : nouveau duc + leger : poids = " +
       * poids_min_sigma_t_ + " t, diametre = " + dia_poids_min_sigma_m_+ " m, epaisseur = " + epais_poids_min_sigma_m_ + "
       * m");
       */
    } // fin if (poids_min_sigma_acc_t_ < poids_min_sigma_t_)
  } // fin methode verifieDucPlusLegerSigma

  /**
   * Methode edite Duc d'Albe le + leger pour une contrainte, un accostage.
   */
  public void editeDucPlusLegerSigmaAcc(final int isigma) {
    System.out.println(" DUC LE PLUS LEGER POUR : CONTRAINTE N�  " + (isigma + 1) + " ; ACCOSTAGE N� " + (iacc + 1)
        + "  avec poids = " + poids_min_sigma_acc_t_ + " t");
    // choix caract. duc + leger contrainte/accostage :
    diametre_m_ = dia_poids_min_sigma_acc_m_;
    epaisseur_act_m_ = epais_poids_min_sigma_acc_m_;
    // appel module de calcul principal :
    calculeDuc();
    // edition :
    editeDuc();
  } // fin editeDucPlusLegerSigmaAcc

  /**
   * Methode edite Duc d'Albe le + leger pour une contrainte, tous les accostages.
   */
  public void editeDucPlusLegerSigma(final int isigma) {
    System.out.println(" DUC LE PLUS LEGER POUR : CONTRAINTE N�  " + (isigma + 1) + " ; TOUS LES ACCOSTAGES ");
    // choix caract. duc + leger contrainte :
    // diametre et epaisseur :
    diametre_m_ = dia_poids_min_sigma_m_;
    epaisseur_act_m_ = epais_poids_min_sigma_m_;
    // accostage : no_acc_poids_min_acc_
    cote_acc_m_ = cotes_acc_m_[no_acc_poids_min_acc_];
    energie_acc_tm_ = energies_acc_tm_[no_acc_poids_min_acc_];
    reaction_acc_t_ = reactions_acc_t_[no_acc_poids_min_acc_];
    // out.println
    System.out.println(" ACCOSTAGE RETENU : ACCOSTAGE N� " + (no_acc_poids_min_acc_ + 1) + " : cote " + cote_acc_m_
        + " m; �nergie " + energie_acc_tm_ + " tm; r�action " + reaction_acc_t_ + " t");
    /*
     * debuggage : System.out.println ("Void editeDucPlusLegerSigma : duc + leger acc : poids = " + poids_min_sigma_t_ + "
     * t, diametre = " + dia_poids_min_sigma_m_+ ", m; epaisseur = " + epais_poids_min_sigma_m_ + " m");
     */
    // appel module de calcul principal :
    calculeDuc();
    // edition :
    editeDuc();
  } // fin editeDucPlusLegerSigma

  /**
   * Methode calcul du Duc d'Albe le + court pour doublet contrainte/accostage.
   */
  public void verifieDucPlusCourtSigmaAcc() {
    if (cote_pied_m_ > cote_pied_min_sigma_acc_m_) {
      // attention : valeurs < 0
      // enregistrement caract. + court :
      cote_pied_min_sigma_acc_m_ = cote_pied_m_;
      dia_pied_min_sigma_acc_m_ = diametre_m_;
      epais_pied_min_sigma_acc_m_ = epaisseur_act_m_;
    } // fin if (cote_pied_m_ > cote_pied_min_sigma_acc_m_)
  } // fin methode verifieDucPlusCourtSigmaAcc

  /**
   * Methode calcul Duc d'Albe le + court pour une contrainte, tous les accostages.
   */
  public void verifieDucPlusCourtAcc() {
    if (cote_pied_min_sigma_acc_m_ > cote_pied_min_sigma_m_) {
      // attention : valeurs < 0
      // enregistrement caract. + court :
      cote_pied_min_sigma_m_ = cote_pied_min_sigma_acc_m_;
      dia_pied_min_sigma_m_ = dia_pied_min_sigma_acc_m_;
      epais_pied_min_sigma_m_ = epais_pied_min_sigma_acc_m_;
      // enregistrement du numero accostage duc le + leger :
      // no_acc_pied_min_acc_
      no_acc_pied_min_acc_ = iacc;
    } // fin if (cote_pied_min_sigma_acc_m_< cote_pied_min_sigma_m_)
  } // fin methode verifieDucPlusCourtAcc

  /**
   * Methode edite Duc d'Albe le + court pour une contrainte, un accostage.
   */
  public void editeDucPlusCourtSigmaAcc(final int isigma) {
    System.out.println(" DUC LE PLUS COURT POUR : CONTRAINTE N�  " + (isigma + 1) + "; ACCOSTAGE N� " + (iacc + 1)
        + "  avec cote pied = " + cote_pied_min_sigma_acc_m_ + " m");
    // choix caract. duc + court contrainte/accostage :
    diametre_m_ = dia_pied_min_sigma_acc_m_;
    epaisseur_act_m_ = epais_pied_min_sigma_acc_m_;
    // appel module de calcul principal :
    calculeDuc();
    // edition :
    editeDuc();
  } // fin editeDucPlusCourtSigmaAcc

  /**
   * Methode edite Duc d'Albe le + court pour une contrainte sigma, tous les accostages.
   */
  public void editeDucPlusCourtSigma(final int isigma) {
    System.out.println(" DUC LE PLUS COURT : CONTRAINTE N�  " + (isigma + 1) + " ; TOUS LES ACCOSTAGES ");
    // choix caract. duc + leger contrainte/accostage :
    diametre_m_ = dia_pied_min_sigma_m_;
    epaisseur_act_m_ = epais_pied_min_sigma_m_;
    // accostage : no_acc_pied_min_acc_
    cote_acc_m_ = cotes_acc_m_[no_acc_pied_min_acc_];
    energie_acc_tm_ = energies_acc_tm_[no_acc_pied_min_acc_];
    reaction_acc_t_ = reactions_acc_t_[no_acc_pied_min_acc_];
    System.out.println(" ACCOSTAGE RETENU : ACCOSTAGE N� " + (no_acc_pied_min_acc_ + 1) + " : cote " + cote_acc_m_
        + " m; �nergie " + energie_acc_tm_ + " tm; r�action " + reaction_acc_t_ + " t; pied " + cote_pied_min_sigma_m_
        + " m");
    // appel module de calcul principal :
    calculeDuc();
    // edition :
    editeDuc();
  } // fin editeDucPlusCourtSigma

  /**
   * Methode editeTableauMomentsLegerSigmaAcc Duc + leger pour une contrainte, un accostage.
   */
  public void editeTableauMomentsLegerSigmaAcc(final int isigma) {
    try {
      outNewLine();
      System.out.println(" TABLEAU DES MOMENTS POUR LE DUC LE PLUS LEGER POUR : CONTRAINTE N�  " + (isigma + 1)
          + " ; ACCOSTAGE N� " + (iacc + 1));
      outNewLine();
      // choix caract. duc + leger :
      diametre_m_ = dia_poids_min_sigma_acc_m_;
      epaisseur_act_m_ = epais_poids_min_sigma_acc_m_;
      // appel module de calcul principal : cote_fiche_m_, force_maxi_t_
      calculeDuc();
      // on impose le premier point : cote_fiche_m_ ou la valeur du moment est
      // nulle
      cotes_moments_leger_m_[0] = cote_fiche_m_;
      moments_leger_tm_[0] = 0.0;
      for (byte i_etml = (byte) (cote_fiche_m_); i_etml <= (byte) (cote_acc_m_); i_etml++) {
        // appel calcul moment sol :
        calculeMomentFlechissant(i_etml, butee_Xm_cmf_t_, moment_sol_tm_, moment_Xm_tm_);
        // calcul bras :
        final double bras_etml_m_ = cote_acc_m_ - i_etml;
        // puis calcul moment total :
        final double _moment_fiche_etml_tm = (force_maxi_t_ * bras_etml_m_) - moment_sol_tm_;
        System.out.println(" moment = " + _moment_fiche_etml_tm + " tm a la cote " + i_etml + " m");
        // affectations pour recup dans resultats :
        cotes_moments_leger_m_[(i_etml - (byte) (cote_fiche_m_ - 1))] = (i_etml);
        moments_leger_tm_[(i_etml - (byte) (cote_fiche_m_ - 1))] = _moment_fiche_etml_tm;
      } // fin boucle for
      outNewLine();
    } // fin try
    catch (final Exception e) {}
  } // fin methode editeTableauMomentsLegerSigmaAcc

  /**
   * Methode edite tableau moments Duc d'Albe le + court pour une contrainte, un accostage.
   */
  public void editeTableauMomentsCourtSigmaAcc(final int isigma) {
    try {
      outNewLine();
      System.out.println(" TABLEAU DES MOMENTS POUR LE DUC LE PLUS COURT POUR CONTRAINTE N�  " + (isigma + 1)
          + " ; ACCOSTAGE N� " + (iacc + 1));
      outNewLine();
      // choix caract. duc + court :
      diametre_m_ = dia_pied_min_sigma_acc_m_;
      epaisseur_act_m_ = epais_pied_min_sigma_acc_m_;
      // appel module de calcul principal : cote_fiche_m_, force_maxi_t_
      calculeDuc();
      // on impose le premier point : cote_fiche_m_ ou la valeur du moment est
      // nulle
      cotes_moments_court_m_[0] = cote_fiche_m_;
      moments_court_tm_[0] = 0.0;
      for (byte i_etmc = (byte) (cote_fiche_m_); i_etmc <= (byte) (cote_acc_m_); i_etmc++) {
        // calcul bras :
        final double bras_etmc_m_ = cote_acc_m_ - i_etmc;
        // appel calcul moment sol. :
        calculeMomentFlechissant(i_etmc, butee_Xm_cmf_t_, moment_sol_tm_, moment_Xm_tm_);
        // calcul moment total :
        final double _moment_fiche_etmc_tm = (force_maxi_t_ * bras_etmc_m_) - moment_sol_tm_;
        System.out.println(" moment = " + _moment_fiche_etmc_tm + " tm a la cote " + i_etmc + " m");
        // affectations pour recup dans resultats :
        cotes_moments_court_m_[(i_etmc - (byte) (cote_fiche_m_ - 1))] = (i_etmc);
        moments_court_tm_[(i_etmc - (byte) (cote_fiche_m_ - 1))] = _moment_fiche_etmc_tm;
      } // fin boucle for
      outNewLine();
    } // fin try
    catch (final Exception e) {}
  } // fin methode editeTableauMomentsCourtSigmaAcc

  /**
   * Methode calcul de l'inertie : 1 appel dans calculeduc.
   * 
   * @param _rEpaisseurCiM
   * @param _rDiametreCiM
   * @param _rNbTubesCi
   * @param _eInertieCiM4
   */
  public void calculeInertie(final double _rEpaisseurCiM, final double _rDiametreCiM, final double _rNbTubesCi,
      double _eInertieCiM4) {
    try {
      // conversions :
      final double _epaisseur_ci_m1 = _rEpaisseurCiM;
      final double _rayon_ci_m1 = _rDiametreCiM * 0.5;
      // puissances :
      final double _rayon_ci_m2 = _rayon_ci_m1 * _rayon_ci_m1;
      final double _rayon_ci_m3 = _rayon_ci_m2 * _rayon_ci_m1;
      final double _epaisseur_ci_m2 = _epaisseur_ci_m1 * _epaisseur_ci_m1;
      final double _epaisseur_ci_m3 = _epaisseur_ci_m2 * _epaisseur_ci_m1;
      // calcul inertie :
      final double _inertie_ci_m3 = ((4 * _rayon_ci_m3) - (6 * _rayon_ci_m2 * _epaisseur_ci_m1)
          + (4 * _rayon_ci_m1 * _epaisseur_ci_m2) - _epaisseur_ci_m3);
      _eInertieCiM4 = _inertie_ci_m3 * Math.PI / 4 * _rNbTubesCi * _epaisseur_ci_m1;
      // affectation variable inertie globale :
      inertie_m4_ = _eInertieCiM4;
      /*
       * debuggage System.out.println ("void calculeInertie : epaisseur = " + _epaisseur_ci_m1 + " m, rayon = " +
       * _rayon_ci_m1 + " m, nb tubes = " + _r_nb_tubes_ci + ", inertie = " + _e_inertie_ci_m4 + " m4");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calcul inertie

  /**
   * Methode calcul du moment maxi admissible : 1 appel dans calculeDuc.
   * 
   * @param _r_diametre_cmm_m
   * @param _r_contrainte_adm_cmm_t
   * @param _r_inertie_cmm_m4
   * @param _e_moment_maxi_adm_cmm_tm
   */
  public void calculeMomentMaxiAdm(final double _r_diametre_cmm_m, final double _r_contrainte_adm_cmm_t,
      final double _r_inertie_cmm_m4, double _e_moment_maxi_adm_cmm_tm) {
    try {
      // calcul moment :
      _e_moment_maxi_adm_cmm_tm = (_r_contrainte_adm_cmm_t * _r_inertie_cmm_m4 * 2) / (_r_diametre_cmm_m);
      // affectation variable moment maxi globale :
      moment_maxi_adm_tm_ = _e_moment_maxi_adm_cmm_tm;
      /*
       * debuggage ok System.out.println ("void calculeMomentMaxiAdm : _contrainte_adm = " + _r_contrainte_adm_cmm_t + "
       * tm, diametre = " + _r_diametre_cmm_m + " m, inertie = " + _r_inertie_cmm_m4 + " m4, moment_maxi_adm_tm_= " +
       * _e_moment_maxi_adm_cmm_tm + " tm");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeMomentMaxiAdm

  /**
   * Methode calcule moment butee sol monocouche.
   * 
   * @param _r_diametre_cm1s_m
   * @param _r_profondeur_cm1s_m
   * @param _e_moment_sol_cm1s_tm
   */
  public void calculeMoment1Sol(final double _r_diametre_cm1s_m, final double _r_profondeur_cm1s_m,
      double _e_moment_sol_cm1s_tm) {
    try {
      // init :
      final double _diametre_cm1s_m1 = _r_diametre_cm1s_m;
      _e_moment_sol_cm1s_tm = 0;
      if (_r_profondeur_cm1s_m < cotes_sup_m_[0]) {
        // dans sol et dans couche 1
        // puissances epaisseurs :
        final double _epais_sol1_cm1s_m1 = cotes_sup_m_[0] - _r_profondeur_cm1s_m;
        final double _epais_sol1_cm1s_m2 = _epais_sol1_cm1s_m1 * _epais_sol1_cm1s_m1;
        final double _epais_sol1_cm1s_m3 = _epais_sol1_cm1s_m2 * _epais_sol1_cm1s_m1;
        final double _epais_sol1_cm1s_m4 = _epais_sol1_cm1s_m2 * _epais_sol1_cm1s_m2;
        // calcul moment butee sol a la profondeur _epais_sol1_cm1s_m1 :
        _e_moment_sol_cm1s_tm = (_diametre_cm1s_m1 * cohesions_t_m2_[0] * _epais_sol1_cm1s_m2)
            + (_diametre_cm1s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cm1s_m3 / 6))
            + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cm1s_m4 / 24));
      } // fin if sol et couche 1
      // affectation variable globale moment_1sol_tm_ :
      moment_1sol_tm_ = _e_moment_sol_cm1s_tm;
      /*
       * debuggage calculeMoment1Sol : System.out.println ("void cm1s : profondeur = " + _r_profondeur_cm1s_m + " m,
       * moment sol = " + _e_moment_sol_cm1s_tm + " tm");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeMoment1Sol

  /**
   * Methode calcule moment butee sol bicouches.
   * 
   * @param _r_diametre_cm2s_m
   * @param _r_profondeur_cm2s_m
   * @param _e_moment_sol_cm2s_tm
   */
  public void calculeMoment2Sols(final double _r_diametre_cm2s_m, final double _r_profondeur_cm2s_m,
      double _e_moment_sol_cm2s_tm) {
    try {
      // init :
      final double _diametre_cm2s_m1 = _r_diametre_cm2s_m;
      _e_moment_sol_cm2s_tm = 0;
      if (_r_profondeur_cm2s_m < cotes_sup_m_[0]) {
        // dans sol
        if (_r_profondeur_cm2s_m >= cotes_sup_m_[1]) {
          // dans couche 1 sur couche 2
          final double _epais_sol1_cm2s_m1 = cotes_sup_m_[0] - _r_profondeur_cm2s_m;
          final double _epais_sol1_cm2s_m2 = _epais_sol1_cm2s_m1 * _epais_sol1_cm2s_m1;
          final double _epais_sol1_cm2s_m3 = _epais_sol1_cm2s_m2 * _epais_sol1_cm2s_m1;
          final double _epais_sol1_cm2s_m4 = _epais_sol1_cm2s_m2 * _epais_sol1_cm2s_m2;
          // calcul moment butee sol a la profondeur _epais_sol1_cm2s_m1 :
          _e_moment_sol_cm2s_tm = (_diametre_cm2s_m1 * cohesions_t_m2_[0] * _epais_sol1_cm2s_m2)
              + (_diametre_cm2s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cm2s_m3 / 6))
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cm2s_m4 / 24));
        } // fin if couche 1
        else {
          // dans couche 2
          // puissances epaisseurs
          final double _epais_sol1_cm2s_m1 = cotes_sup_m_[0] - cotes_sup_m_[1];
          final double _epais_sol2_cm2s_m1 = cotes_sup_m_[1] - _r_profondeur_cm2s_m;
          final double _epais_sol1_cm2s_m2 = _epais_sol1_cm2s_m1 * _epais_sol1_cm2s_m1;
          final double _epais_sol1_cm2s_m3 = _epais_sol1_cm2s_m2 * _epais_sol1_cm2s_m1;
          final double _epais_sol1_cm2s_m4 = _epais_sol1_cm2s_m2 * _epais_sol1_cm2s_m2;
          final double _epais_sol2_cm2s_m2 = _epais_sol2_cm2s_m1 * _epais_sol2_cm2s_m1;
          final double _epais_sol2_cm2s_m3 = _epais_sol2_cm2s_m2 * _epais_sol2_cm2s_m1;
          final double _epais_sol2_cm2s_m4 = _epais_sol2_cm2s_m2 * _epais_sol2_cm2s_m2;
          // appel calculeButee1Sol :
          calculeButee1Sol(_r_diametre_cm2s_m, cotes_sup_m_[1], butee_1sol_t_);
          // calcul moment butee sol a la profondeur _epais_sol1_cm2s_m1 :
          _e_moment_sol_cm2s_tm = (_diametre_cm2s_m1 * cohesions_t_m2_[0] * _epais_sol1_cm2s_m2)
              + (_diametre_cm2s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_cm2s_m3 / 6)
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cm2s_m4 / 24))
              + (_diametre_cm2s_m1 * cohesions_t_m2_[1] * _epais_sol2_cm2s_m2)
              + (_diametre_cm2s_m1 * poids_sol_t_m3_[1] * coeffs_butee_[1] * _epais_sol2_cm2s_m3 / 6)
              + (poids_sol_t_m3_[1] * coeffs_butee_[1] * (_epais_sol2_cm2s_m4 / 24))
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol2_cm2s_m2 * (_epais_sol1_cm2s_m2 / 4))
              + (_diametre_cm2s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_cm2s_m1 * (_epais_sol2_cm2s_m2 / 2))
              + (poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_cm2s_m1 * (_epais_sol2_cm2s_m3 / 6))
              + (butee_1sol_t_ * _epais_sol2_cm2s_m1);
        } // fin else couche 2
      } // fin if sol
      // affectation variable globale moment_2sols_tm_ :
      moment_2sols_tm_ = _e_moment_sol_cm2s_tm;
      /*
       * debuggage calculeMoment2Sols : System.out.println ("void calculeMoment2Sols : profondeur = " +
       * _r_profondeur_cm2s_m + " m, butee = " + coeffs_butee_[0] + " moment sol = " + _e_moment_sol_cm2s_tm + " tm");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeMoment2Sols

  /**
   * Methode calcul butee a la cote _r_profondeur_cb1s_m dans un sol monocouche. attention : 0> parametre
   * _r_profondeur_cb1s_m ;
   * 
   * @param _r_diametre_cb1s_m
   * @param _r_profondeur_cb1s_m
   * @param _e_butee_Xm_cb1s_t
   */
  public void calculeButee1Sol(final double _r_diametre_cb1s_m, final double _r_profondeur_cb1s_m,
      double _e_butee_Xm_cb1s_t) {
    try {
      // init et conv.
      final double _diametre_cb1s_m1 = _r_diametre_cb1s_m;
      _e_butee_Xm_cb1s_t = 0;
      if (_r_profondeur_cb1s_m < cotes_sup_m_[0]) {
        // dans sol et dans couche 1
        // puissances epaisseurs :
        final double _epais_sol1_cb1s_m1 = cotes_sup_m_[0] - _r_profondeur_cb1s_m;
        final double _epais_sol1_cb1s_m2 = _epais_sol1_cb1s_m1 * _epais_sol1_cb1s_m1;
        final double _epais_sol1_cb1s_m3 = _epais_sol1_cb1s_m2 * _epais_sol1_cb1s_m1;
        // calcul _e_butee_Xm_cb1s_t du sol dans couche 1 en Xm a la profondeur
        // _epais_sol1_cb1s_m1
        // :
        _e_butee_Xm_cb1s_t = (2 * _diametre_cb1s_m1 * cohesions_t_m2_[0] * _epais_sol1_cb1s_m1)
            + (_diametre_cb1s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb1s_m2 / 2))
            + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb1s_m3 / 6));
      } // fin if calcul butee dans sol : couche 1
      // affectation variable globale butee_1sol_t_ :
      butee_1sol_t_ = _e_butee_Xm_cb1s_t;
      /*
       * debuggage calculeButee1Sol System.out.println ("void calculeButee1Sol : profondeur = " + _r_profondeur_cb1s_m + "
       * m, butee = " + coeffs_butee_[0] + " butee sol = " + _e_butee_Xm_cb1s_t + " tm");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeButee1Sol

  /**
   * Methode calcul de la butee a la cote _r_profondeur_cb2s_m dans sol bicouche. attention : 0>parametre
   * _r_profondeur_cb2s_m
   * 
   * @param _r_diametre_cb2s_m
   * @param _r_profondeur_cb2s_m
   * @param _e_butee_Xm_cb2s_t
   */
  public void calculeButee2Sols(final double _r_diametre_cb2s_m, final double _r_profondeur_cb2s_m,
      double _e_butee_Xm_cb2s_t) {
    try {
      // initialisations :
      final double _diametre_cb2s_m = _r_diametre_cb2s_m;
      _e_butee_Xm_cb2s_t = 0;
      if (_r_profondeur_cb2s_m < cotes_sup_m_[0]) {
        // dans sol
        if (_r_profondeur_cb2s_m >= cotes_sup_m_[1]) {
          // dans couche 1 sur couche 2
          // puissances epaisseurs :
          final double _epais_sol1_cb2s_m1 = cotes_sup_m_[0] - _r_profondeur_cb2s_m;
          final double _epais_sol1_cb2s_m2 = _epais_sol1_cb2s_m1 * _epais_sol1_cb2s_m1;
          final double _epais_sol1_cb2s_m3 = _epais_sol1_cb2s_m2 * _epais_sol1_cb2s_m1;
          // calcul _e_butee_Xm_cb2s_t du sol dans couche 1 en Xm a la
          // profondeur
          // _epais_sol1_cb2s_m1 :
          _e_butee_Xm_cb2s_t = (2 * _diametre_cb2s_m * cohesions_t_m2_[0] * _epais_sol1_cb2s_m1)
              + (_diametre_cb2s_m * poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb2s_m2 / 2))
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb2s_m3 / 6));
        } // fin if calcul butee dans couche 1
        else {
          // dans couche 2
          // puissances epaisseurs :
          final double _epais_sol1_cb2s_m1 = cotes_sup_m_[0] - cotes_sup_m_[1];
          final double _epais_sol2_cb2s_m1 = cotes_sup_m_[1] - _r_profondeur_cb2s_m;
          final double _epais_sol1_cb2s_m2 = _epais_sol1_cb2s_m1 * _epais_sol1_cb2s_m1;
          final double _epais_sol1_cb2s_m3 = _epais_sol1_cb2s_m2 * _epais_sol1_cb2s_m1;
          final double _epais_sol2_cb2s_m2 = _epais_sol2_cb2s_m1 * _epais_sol2_cb2s_m1;
          final double _epais_sol2_cb2s_m3 = _epais_sol2_cb2s_m2 * _epais_sol2_cb2s_m1;
          // calcul _e_butee_Xm_cb2s_t du sol dans couche 2 a la
          // _r_profondeur_cb2s_m =
          // _epais_sol2_cb2s_m1 :
          _e_butee_Xm_cb2s_t = (_diametre_cb2s_m * poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb2s_m2 / 2))
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_cb2s_m3 / 6))
              + (2 * _diametre_cb2s_m * cohesions_t_m2_[0] * _epais_sol1_cb2s_m1)
              + (poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_cb2s_m2 * (_epais_sol2_cb2s_m1 / 2))
              + (_diametre_cb2s_m * poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_cb2s_m1 * _epais_sol2_cb2s_m1)
              + (_diametre_cb2s_m * poids_sol_t_m3_[1] * coeffs_butee_[1] * (_epais_sol2_cb2s_m2 / 2))
              + (poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_cb2s_m1 * (_epais_sol2_cb2s_m2 / 2))
              + (poids_sol_t_m3_[1] * coeffs_butee_[1] * (_epais_sol2_cb2s_m3 / 6))
              + (2 * _diametre_cb2s_m * cohesions_t_m2_[1] * _epais_sol2_cb2s_m1);
        } // fin else calcul butee dans couche 2
      } // fin if calcul butee dans sol
      // affectation variable globale butee_2sols_t_ :
      butee_2sols_t_ = _e_butee_Xm_cb2s_t;
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeButee2Sols

  // ***************************************************************************
  //
  // ***************************************************************************
  //
  /**
   * methode calcul derivee de la butee a la cote _r_profondeur_db1s_m1; sol monocouche. attention : 0>parametres
   * _r_profondeur_db1s_m1 et cotes_sup_m_[0].
   * 
   * @param _r_diametre_db1s_m
   * @param _r_profondeur_db1s_m1
   * @param _e_derivee_butee_db1s_t_m
   */
  public void calculeDeriveeButee1Sol(final double _r_diametre_db1s_m, final double _r_profondeur_db1s_m1,
      double _e_derivee_butee_db1s_t_m) {
    try {
      // initialisations et conv. :
      final double _diametre_db1s_m1 = _r_diametre_db1s_m;
      _e_derivee_butee_db1s_t_m = 0;
      if (_r_profondeur_db1s_m1 < cotes_sup_m_[0]) {
        // dans sol monocouche
        // puissances epaisseurs :
        final double _epais_sol1_db1s_m1 = cotes_sup_m_[0] - _r_profondeur_db1s_m1;
        final double _epais_sol1_db1s_m2 = _epais_sol1_db1s_m1 * _epais_sol1_db1s_m1;
        // calcul derivee butee
        _e_derivee_butee_db1s_t_m = (poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_db1s_m2 / 2)
            + (_diametre_db1s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_db1s_m1)
            + (2 * _diametre_db1s_m1 * cohesions_t_m2_[0]);
      } // fin if calcul derive butee dans sol monocouche
      // affectation variable globale derivee_butee_1sol_t_m_ :
      derivee_butee_1sol_t_m_ = _e_derivee_butee_db1s_t_m;
      /*
       * debuggage calculeDeriveeButee1Sol System.out.println ("void calculeDeriveeButee1Sol : profondeur = " +
       * _r_profondeur_db1s_m1 + " m, butee = " + coeffs_butee_[0] + " derivee_butee en t/m = " +
       * _e_derivee_butee_db1s_t_m);
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeDeriveeButee1Sol

  /**
   * Methode calcul derivee de la butee a la cote _r_profondeur_db2s_m1, sol bicouches. attention : 0>parametre
   * _r_profondeur_db2s_m1
   * 
   * @param _r_diametre_db2s_m
   * @param _r_profondeur_db2s_m1
   * @param _e_derivee_butee_db2s_t_m
   */
  public void calculeDeriveeButee2Sols(final double _r_diametre_db2s_m, final double _r_profondeur_db2s_m1,
      double _e_derivee_butee_db2s_t_m) {
    try {
      // initialisations :
      final double _diametre_db2s_m1 = _r_diametre_db2s_m;
      _e_derivee_butee_db2s_t_m = 0;
      if (_r_profondeur_db2s_m1 < cotes_sup_m_[0]) {
        // dans sol
        if (_r_profondeur_db2s_m1 >= cotes_sup_m_[1]) {
          // dans couche 1 sur couche 2
          // puissances epaisseurs :
          final double _epais_sol1_db2s_m1 = cotes_sup_m_[0] - _r_profondeur_db2s_m1;
          final double _epais_sol1_db2s_m2 = _epais_sol1_db2s_m1 * _epais_sol1_db2s_m1;
          // calcul derivee butee
          _e_derivee_butee_db2s_t_m = (poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_db2s_m2 / 2)
              + (_diametre_db2s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[0] * _epais_sol1_db2s_m1)
              + (2 * _diametre_db2s_m1 * cohesions_t_m2_[0]);
        } // fin if calcul derive butee dans couche 1
        else {
          // dans couche 2
          final double _epais_sol1_db2s_m1 = cotes_sup_m_[0] - cotes_sup_m_[1];
          final double _epais_sol2_db2s_m1 = cotes_sup_m_[1] - _r_profondeur_db2s_m1;
          final double _epais_sol1_db2s_m2 = _epais_sol1_db2s_m1 * _epais_sol1_db2s_m1;
          final double _epais_sol2_db2s_m2 = _epais_sol2_db2s_m1 * _epais_sol2_db2s_m1;
          // calcul de la derivee de la butee du sol en Xm : (2 couches de sol)
          _e_derivee_butee_db2s_t_m = (poids_sol_t_m3_[0] * coeffs_butee_[0] * (_epais_sol1_db2s_m2 / 2))
              + (_diametre_db2s_m1 * poids_sol_t_m3_[1] * coeffs_butee_[1] * _epais_sol2_db2s_m1)
              + (poids_sol_t_m3_[1] * coeffs_butee_[1] * (_epais_sol2_db2s_m2 / 2))
              + (_diametre_db2s_m1 * poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_db2s_m1)
              + (poids_sol_t_m3_[0] * coeffs_butee_[1] * _epais_sol1_db2s_m1 * _epais_sol2_db2s_m1)
              + (2 * _diametre_db2s_m1 * cohesions_t_m2_[1]);
        } // fin if calcul derivee butee dans couche 2
      } // fin if calcul derivee butee dans sol
      // affectation variable globale derivee_butee_2sols_t_m_ :
      derivee_butee_2sols_t_m_ = _e_derivee_butee_db2s_t_m;
      /*
       * debuggage calculeDeriveeButee2Sols System.out.println ("void calculeDeriveeButee2Sols : profondeur = " +
       * _r_profondeur_db2s_m1 + " m, butee = " + coeffs_butee_[0] + " derivee_butee en t/m = " +
       * _e_derivee_butee_db2s_t_m);
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeDeriveeButee2Sols

  /**
   * Methode calcul du poids de l'ouvrage : 1 appel dans calculeCoteFiche.
   * 
   * @param _r_diametre_cpd_m
   * @param _r_epaisseur_cpd_m
   * @param _r_hauteur_cpd_m
   * @param _e_poids_cpd_t
   */
  public void calculePoidsDuc(final double _r_diametre_cpd_m, final double _r_epaisseur_cpd_m,
      final double _r_hauteur_cpd_m, double _e_poids_cpd_t) {
    final double _diametre_cpd_m = _r_diametre_cpd_m;
    final double _epaisseur_tot_cpd_m = _r_epaisseur_cpd_m;
    final double section_cpd_m2_ = Math.PI * _epaisseur_tot_cpd_m * (_diametre_cpd_m - _epaisseur_tot_cpd_m);
    final double volume_cpd_m3_ = section_cpd_m2_ * _r_hauteur_cpd_m;
    _e_poids_cpd_t = volume_cpd_m3_ * POIDS_ACIER_T_M3;
    // poids_acier en constante
    // affectation variable globale poids_duc_t_
    poids_duc_t_ = _e_poids_cpd_t;
    /*
     * debuggage calculePoids : System.out.println ("diametre dans void calculePoids = " + _r_diametre_cpd_m + " m");
     * System.out.println ("epaisseur dans void calculePoids = " + _epaisseur_tot_cpd_m + " m"); System.out.println
     * ("volume dans void calculePoids = " + volume_cpd_m3_ + " m3"); System.out.println ("hauteur dans void
     * calculePoids = " + _r_hauteur_cpd_m + " m"); System.out.println ("poids dans void calculePoids = " +
     * _e_poids_cpd_t + " t"); outNewLine();
     */
  } // fin methode calculePoidsDuc

  /**
   * Methode calculeFleches au choc et en tete : 1 appel dans calculeDuc.
   * 
   * @param _r_fiche_cf_m
   * @param _r_force_maxi_cf_t
   * @param _r_inertie_cf_m4_
   * @param _e_fleche_choc_cf_m
   * @param _e_fleche_tete_cf_m
   */
  public void calculeFleches(final double _r_fiche_cf_m, final double _r_force_maxi_cf_t,
      final double _r_inertie_cf_m4_, double _e_fleche_choc_cf_m, double _e_fleche_tete_cf_m) {
    final double _fiche_eq_cf_m1 = cote_acc_m_ + (0.78 * _r_fiche_cf_m) - cotes_sup_m_[0];
    final double _fiche_eq_cf_m2 = _fiche_eq_cf_m1 * _fiche_eq_cf_m1;
    final double _fiche_eq_cf_m3 = _fiche_eq_cf_m2 * _fiche_eq_cf_m1;
    // calcul fleches choc :
    _e_fleche_choc_cf_m = (_r_force_maxi_cf_t * _fiche_eq_cf_m3) / (3 * 1E6 * young_Mt_m2_ * _r_inertie_cf_m4_);
    // calcul fleche tete :
    final double _rotat_choc_cf = (_r_force_maxi_cf_t * _fiche_eq_cf_m2) / (2 * 1E6 * young_Mt_m2_ * _r_inertie_cf_m4_);
    _e_fleche_tete_cf_m = _e_fleche_choc_cf_m + (_rotat_choc_cf * (cote_tete_m_ - cote_acc_m_));
    // affectations variables globales fleches :
    fleche_choc_m_ = _e_fleche_choc_cf_m;
    fleche_tete_m_ = _e_fleche_tete_cf_m;
    /*
     * debuggage calculeFleche : System.out.println ("fiche dans void calculeFleches = " + _r_fiche_cf_m + " m");
     * System.out.println ("force dans void calculeFleches = " + _r_force_maxi_cf_t + " m"); System.out.println ("fleche
     * choc dans void calculeFleches = " + _e_fleche_choc_cf_m + " m a la cote " + cote_acc_m_ + " m"); //
     * System.out.println ("fleche tete dans void calculeFleches = " + _e_fleche_tete_cf_m + " m a la c�te " +
     * cote_tete_m_+ " m");
     */
  } // fin methode calculeFleches

  /**
   * Methode calculeEnergieTubes : 1 appel dans calculeDuc.
   * 
   * @param _r_force_maxi_cep_t
   * @param _r_fleche_choc_cep_m
   * @param _e_energie_tubes_cep_tm
   */
  public void calculeEnergieTubes(final double _r_force_maxi_cep_t, final double _r_fleche_choc_cep_m,
      double _e_energie_tubes_cep_tm) {
    // calcul energies :
    _e_energie_tubes_cep_tm = (_r_force_maxi_cep_t * _r_fleche_choc_cep_m * 0.5);
    // affectations variable globale energie_tubes_tm_ :
    energie_tubes_tm_ = _e_energie_tubes_cep_tm;
    /*
     * debuggage calculeEnergieTubes : System.out.println ("energie absorbee par tubes dans void calculeEnergieTubes = " +
     * _e_energie_tubes_cep_tm + " tm"); System.out.println ("fleche choc dans void calculeEnergieTubes = " +
     * _r_fleche_choc_cep_m + " m a la cote " + cote_acc_m_+ " m");
     */
  } // fin methode calculeEnergieTubes

  /**
   * Methode calcule energie absorbee par defense soumise a une force _r_force_maxi_ced_t : 1 appel dans calculeDuc.
   * 
   * @param _r_force_maxi_ced_t
   * @param _e_deflexion_ced_pc
   * @param _e_energie_defense_ced_tm
   */
  public void calculeEnergieDefense(final double _r_force_maxi_ced_t, double _e_deflexion_ced_pc,
      double _e_energie_defense_ced_tm) {
    try {
      // initialisations x: def ; r:y:efforts
      double rg_ = 0.0;
      // initialisations x: def ; r:y:efforts
      double xg_ = 0.0;
      _e_deflexion_ced_pc = 0.0;
      _e_energie_defense_ced_tm = 0.0;
      pointsED_S_[0][0] = 0.0;
      pointsED_S_[0][1] = 0.0; // ajout force du point 1 : (0,0)
      nb_pointsED_S_ = 1;
      // 2 conditions : 1 courbe ED (au - 1 point) et choc dans intervalle de
      // validite :
      if (nb_pointsED_E_ > 0) {
        // existence defense
        for (byte i_ced = 0; i_ced < nb_pointsED_E_; i_ced++) {
          nb_pointsED_S_ = (i_ced + 2);
          if (_r_force_maxi_ced_t <= pointsED_E_[i_ced][1]) {
            // sur segment:derniere surface
            final double num_pente_ced_ = (pointsED_E_[i_ced][1] - rg_);
            final double den_pente_ced_ = (pointsED_E_[i_ced][0] - xg_);
            final double pente_ced_ = num_pente_ced_ / den_pente_ced_;
            _e_deflexion_ced_pc = xg_ + ((_r_force_maxi_ced_t - rg_) / pente_ced_);
            _e_energie_defense_ced_tm = _e_energie_defense_ced_tm
                + (0.005 * (_e_deflexion_ced_pc - xg_) * (_r_force_maxi_ced_t - rg_) * (hauteur_defense_m_))
                + (0.01 * rg_ * (_e_deflexion_ced_pc - xg_) * (hauteur_defense_m_));
            pointsED_S_[(i_ced + 1)][0] = _e_deflexion_ced_pc;
            pointsED_S_[(i_ced + 1)][1] = _r_force_maxi_ced_t;
            break; // sortie de boucle
          } // fin if (_r_force_maxi_ced_t < pointsED_E_[i_ced] [1])
          else if (_r_force_maxi_ced_t > pointsED_E_[i_ced][1]) {
            // surface trapeze precedent
            _e_energie_defense_ced_tm = _e_energie_defense_ced_tm
                + (0.005 * (pointsED_E_[i_ced][0] - xg_) * (pointsED_E_[i_ced][1] - rg_) * (hauteur_defense_m_))
                + (0.01 * rg_ * (pointsED_E_[i_ced][0] - xg_) * (hauteur_defense_m_));
            pointsED_S_[(i_ced + 1)][0] = pointsED_E_[i_ced][0];
            pointsED_S_[(i_ced + 1)][1] = pointsED_E_[i_ced][1];
          } // fin else if
          rg_ = pointsED_E_[i_ced][1];
          xg_ = pointsED_E_[i_ced][0];
        } // fin boucle for byte i_ced
      } // fin if (nb_pointsED_E > 0);
      // verification cote accostage dans intervalle de validite :
      if (iv_) {
        if (cote_mini_IV_m_ > cote_acc_m_) {
          _e_deflexion_ced_pc = 0.0;
          _e_energie_defense_ced_tm = 0.0;
          nb_pointsED_S_ = 0;
          pointsED_S_ = null;
        }
        if (cote_maxi_IV_m_ < cote_acc_m_) {
          _e_deflexion_ced_pc = 0.0;
          _e_energie_defense_ced_tm = 0.0;
          nb_pointsED_S_ = 0;
        }
      } // fin if ();
      // affectations variables globales :
      energie_defense_tm_ = _e_energie_defense_ced_tm;
      deflexion_pc_ = _e_deflexion_ced_pc;
      /*
       * debuggage calculeEnergieDefense : System.out.println ("energie absorbee d�fense dans calculeEnergiedefense = " +
       * _e_energie_defense_ced_tm + " tm"); System.out.println ("_e_deflexion_ced_pc dasn calculeEnergiedefense = " +
       * _e_deflexion_ced_pc + " % ");
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeEnergieDefense

  /**
   * Methode calculeMomentFlechissant : nombreux appels.
   * 
   * @param _r_profondeur_cmf_m
   * @param _e_butee_Xm_cmf_t
   * @param _e_moment_sol_cmf_tm
   * @param _e_moment_Xm_cmf_tm
   */
  public void calculeMomentFlechissant(final double _r_profondeur_cmf_m, double _e_butee_Xm_cmf_t,
      double _e_moment_sol_cmf_tm, double _e_moment_Xm_cmf_tm) {
    try {
      // r_profondeur_cmf_m < 0 !
      if (nb_sols_ == 1) {
        // calcul butee et moment 1 sol :
        calculeButee1Sol(diametre_m_, _r_profondeur_cmf_m, butee_1sol_t_);
        calculeMoment1Sol(diametre_m_, _r_profondeur_cmf_m, moment_1sol_tm_);
        // affectation butee et moment sol : 1 sol :
        _e_butee_Xm_cmf_t = butee_1sol_t_;
        _e_moment_sol_cmf_tm = moment_1sol_tm_;
      } // fin nb_sols_= 1
      else {
        // nb_sols_= 2
        // calcul butee et moment 2 sols :
        calculeButee2Sols(diametre_m_, _r_profondeur_cmf_m, butee_2sols_t_);
        calculeMoment2Sols(diametre_m_, _r_profondeur_cmf_m, moment_2sols_tm_);
        // affectation butee et moment sol : 2 sols :
        _e_butee_Xm_cmf_t = butee_2sols_t_;
        _e_moment_sol_cmf_tm = moment_2sols_tm_;
      } // fin nb_sols_= 2
      // calcul moment fl. :
      final double bras_cmf_m_ = (cote_acc_m_ - _r_profondeur_cmf_m);
      _e_moment_Xm_cmf_tm = (_e_butee_Xm_cmf_t * bras_cmf_m_) - _e_moment_sol_cmf_tm;
      // affectations :
      butee_Xm_cmf_t_ = _e_butee_Xm_cmf_t;
      moment_sol_tm_ = _e_moment_sol_cmf_tm;
      moment_Xm_tm_ = _e_moment_Xm_cmf_tm;
      /*
       * debuggage calculeMomentFlechissant : System.out.println (" void cmf : prof = " + _r_profondeur_cmf_m + " m;
       * moment_Xm = " + _e_moment_Xm_cmf_tm + " tm; moment sol = " + _e_moment_sol_cmf_tm + " tm; butee_Xm = " +
       * _e_butee_Xm_cmf_t + " t, bras en m = " + bras_cmf_m_); //
       */
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeMomentFlechissant

  /**
   * Methode calculeForceMaxi applicable a l'ouvrage : 1 appel dans calculeDuc : v 1.04.
   * 
   * @param _r_diametre_cfm_m
   * @param _r_moment_maxi_cfm_tm
   * @param _e_force_maxi_cfm_t
   * @param _e_cote_moment_maxi_cfm_m
   * @param _e_moment_Xm_cmf_tm
   * @param _e_convergence_moment_cfm
   */
  public void calculeForceMaxi(final double _r_diametre_cfm_m, final double _r_moment_maxi_cfm_tm,
      double _e_force_maxi_cfm_t, double _e_cote_moment_maxi_cfm_m, double _e_moment_Xm_cmf_tm,
      boolean _e_convergence_moment_cfm) {
    try {
      double derivee_butee_cfm_t_m_;
      double bras_cfm_m_;
      double _deriv_cfm_t;
      double _prof_force_maxi_cfm_m;
      double _delta_prof_moment_cfm_m;
      // initialisations :
      _e_convergence_moment_cfm = false;
      double _prof_cfm_m = prof_init_force_m_; // prof. init constante
      byte i_cfm;
      // iteration sur le moment maxi:
      for (i_cfm = 1; i_cfm < NB_ITER; i_cfm++) {
        // test de convergence moment total :
        if (nb_sols_ == 1) {
          // 1 couche de sol
          calculeDeriveeButee1Sol(_r_diametre_cfm_m, _prof_cfm_m, derivee_butee_1sol_t_m_);
          derivee_butee_cfm_t_m_ = derivee_butee_1sol_t_m_;
        } else {
          // 2 couches de sol
          calculeDeriveeButee2Sols(_r_diametre_cfm_m, _prof_cfm_m, derivee_butee_2sols_t_m_);
          derivee_butee_cfm_t_m_ = derivee_butee_2sols_t_m_;
        }
        if (derivee_butee_cfm_t_m_ == 0) {
          System.out.println("erreur : derivee butee nulle. Arret du calcul de la force ");
          break; // sortie boucle
        } // fin (derivee_butee_cfm_t_m_ = 0)
        // if derivee_butee_cfm_t_m_ =! 0
        // calcul butee_Xm_cmf_t_, moment_Xm_tm_ :
        calculeMomentFlechissant(_prof_cfm_m, butee_Xm_cmf_t_, moment_sol_tm_, moment_Xm_tm_);
        // calcul _deriv_cfm_t :
        bras_cfm_m_ = cote_acc_m_ - _prof_cfm_m; // bras > 0 :
        _deriv_cfm_t = derivee_butee_cfm_t_m_ * bras_cfm_m_;
        // algorithme convergence :
        _delta_prof_moment_cfm_m = ((moment_Xm_tm_ - _r_moment_maxi_cfm_tm) / _deriv_cfm_t);
        _prof_force_maxi_cfm_m = _prof_cfm_m + _delta_prof_moment_cfm_m;
        if (_delta_prof_moment_cfm_m < 0) {
          // conversion en valeur positive
          _delta_prof_moment_cfm_m = -_delta_prof_moment_cfm_m;
        }
        if (_delta_prof_moment_cfm_m <= PREC_PROF_M) {
          // convergence
          // affectations variables locales :
          _e_convergence_moment_cfm = true;
          _e_cote_moment_maxi_cfm_m = _prof_force_maxi_cfm_m;
          _e_force_maxi_cfm_t = butee_Xm_cmf_t_;
          _e_moment_Xm_cmf_tm = moment_Xm_tm_;
          // affectations variables globales :
          convergence_moment_ = _e_convergence_moment_cfm;
          force_maxi_t_ = _e_force_maxi_cfm_t;
          cote_moment_maxi_m_ = _e_cote_moment_maxi_cfm_m;
          moment_maxi_tm_ = _e_moment_Xm_cmf_tm;
          /*
           * debuggage calculeForceMaxi System.out.println (" void calculeForceMaxi conv. moment : contrainte = " +
           * contrainte_adm_t_m2_ + "t/m2; diam�tre = " + _r_diametre_cfm_m+ " m; �paisseur = " + epaisseur_act_m_+ "
           * m"); System.out.println ("moment maxi = " + moment_maxi_tm_+ " tm a la cote " + cote_moment_maxi_m_+ " m
           * avec force maxi applic. = " + force_maxi_t_ + " t");
           */
          break;
        } // fin_delta_prof_moment_cfm_m <= prec_prof_m_
        // if (_delta_prof_moment_cfm_m <= prec_prof_m_)
        _prof_cfm_m = _prof_force_maxi_cfm_m;
        // fin else if derivee_butee_cfm_t_m_ =! 0
      } // fin boucle for i_cfm
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeForceMaxi

  /**
   * Methode calculeCoteFiche : profondeur du moment nul (contre-butee) : 1 appel dans calculeDuc.
   * 
   * @param _r_force_maxi_ccf_t
   * @param _e_cote_fiche_ccf_m
   * @param _e_convergence_fiche_ccf
   */
  public void calculeCoteFiche(final double _r_force_maxi_ccf_t, double _e_cote_fiche_ccf_m,
      boolean _e_convergence_fiche_ccf) {
    try {
      double _bras_ccf_m;
      double _moment_fiche_ccf_tm;
      double _delta_prof_fiche_ccf_m;
      double _cote_fiche_ccf_m;
      // initialisations : _e_cote_fiche_ccf_m < 0 :
      _e_cote_fiche_ccf_m = prof_init_fiche_m_;
      _e_convergence_fiche_ccf = false;
      // iteration sur la cote fiche :
      byte i_ccf;
      for (i_ccf = 1; i_ccf < NB_ITER; i_ccf++) {
        // nb_iter constante
        // calcul butee_Xm_cmf_t_ et moment_sol_tm_ en _e_cote_fiche_ccf_m :
        _bras_ccf_m = cote_acc_m_ - _e_cote_fiche_ccf_m;
        calculeMomentFlechissant(_e_cote_fiche_ccf_m, butee_Xm_cmf_t_, moment_sol_tm_, moment_Xm_tm_);
        _moment_fiche_ccf_tm = (_r_force_maxi_ccf_t * _bras_ccf_m) - moment_sol_tm_;
        // pour test de convergence fiche : repris ancien dimduc
        _delta_prof_fiche_ccf_m = (_moment_fiche_ccf_tm) / (_r_force_maxi_ccf_t - butee_Xm_cmf_t_);
        _cote_fiche_ccf_m = _e_cote_fiche_ccf_m + _delta_prof_fiche_ccf_m;
        // < 0
        if (_delta_prof_fiche_ccf_m < 0) {
          // valeur positive pour test convergence
          _delta_prof_fiche_ccf_m = -_delta_prof_fiche_ccf_m;
        }
        if (_delta_prof_fiche_ccf_m <= PREC_PROF_M) {
          // precision : constante
          // convergence prof fiche
          _e_convergence_fiche_ccf = true;
          // affectation variables globales cote_fiche_m_ :
          convergence_fiche_ = _e_convergence_fiche_ccf;
          cote_fiche_m_ = _e_cote_fiche_ccf_m;
          /*
           * debuggage calculeCoteFiche System.out.println (" "); System.out.println ("convergence fiche dans cf a
           * l'iteration n� " + i_ccf); System.out.println ("cote fiche = " + _e_cote_fiche_ccf_m + " m, moment fiche = " +
           * _moment_fiche_ccf_tm + "tm, force = " + _r_force_maxi_ccf_t + " t");
           */
          break; // sortie boucle
        } // fin delta_prof_fiche <= prec_prof_m_
        // if (_delta_prof_fiche_ccf_m > prec_prof_m_) : non convergence prof
        // fiche
        _e_cote_fiche_ccf_m = _cote_fiche_ccf_m;
        // System.out.println ("cote fiche m = " + _e_cote_fiche_ccf_m);
      } // fin boucle for
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeCoteFiche

  /**
   * methode calculeCoteMomentMaxi : profondeur du moment maxi : a partir egalite efforts : f(sol) = (butee_Xm_cmf_t_) =
   * force maxi: 1 appel dans calculeDuc v 1.05.
   * 
   * @param _r_cote_fiche_ccmm_m
   * @param _r_force_maxi_ccmm_t
   * @param _e_moment_maxi_ccmm_tm
   * @param _e_cote_moment_maxi_ccmm_m
   * @param _e_convergence_cote_ccmm
   */
  public void calculeCoteMomentMaxi(final double _r_cote_fiche_ccmm_m, final double _r_force_maxi_ccmm_t,
      double _e_moment_maxi_ccmm_tm, double _e_cote_moment_maxi_ccmm_m, boolean _e_convergence_cote_ccmm) {
    try {
      // initialisations : _e_cote_fiche_ccf_m < 0 et ans sol ! :
      double _delta_prof_cote_ccmm_m;
      _e_cote_moment_maxi_ccmm_m = (cotes_sup_m_[0] + _r_cote_fiche_ccmm_m) / 2;
      // < 0
      double _cote_moment_ccmm_m = _e_cote_moment_maxi_ccmm_m;
      _e_convergence_cote_ccmm = false;
      // iteration sur la cote moment maxi :
      byte i_ccmm;
      for (i_ccmm = 1; i_ccmm < NB_ITER; i_ccmm++) {
        // nb_iter constante
        // calcul butee_Xm_cmf_t_ et moment_sol_tm_ en
        // _e_cote_moment_maxi_ccmm_m :
        calculeMomentFlechissant(_e_cote_moment_maxi_ccmm_m, butee_Xm_cmf_t_, moment_sol_tm_, moment_Xm_tm_);
        if (moment_Xm_tm_ == 0) {
          System.out.println(" moment flechissant nul : en dehors sol : sortie boucle iteration n� " + i_ccmm);
          break;
        }
        // pour test de convergence prof cote :
        _delta_prof_cote_ccmm_m = (-(_r_force_maxi_ccmm_t - butee_Xm_cmf_t_) * 40) / (moment_Xm_tm_ * i_ccmm);
        // * ccmm
        /*
         * System.out.println ("_delta_prof_cote_ccmm_m � iteration n� = " + i_ccmm + " = " + _delta_prof_cote_ccmm_m + "
         * m, cote moment = " + _e_cote_moment_maxi_ccmm_m + "m"); System.out.println ("but�e sol = " + butee_Xm_cmf_t_ +
         * "t, moment sol tm = + " + moment_sol_tm_);
         */
        _cote_moment_ccmm_m = _e_cote_moment_maxi_ccmm_m + _delta_prof_cote_ccmm_m;
        // < 0
        if (Math.abs(_delta_prof_cote_ccmm_m) <= PREC_PROF_M) {
          // precision : constante
          // convergence prof fiche
          _e_convergence_cote_ccmm = true;
          _e_moment_maxi_ccmm_tm = moment_Xm_tm_;
          // affectation variables globales cote_fiche_m_ :
          convergence_cote_ = _e_convergence_cote_ccmm;
          moment_maxi_tm_ = _e_moment_maxi_ccmm_tm;
          cote_moment_maxi_m_ = _e_cote_moment_maxi_ccmm_m;
          /*
           * debuggage calculeCoteMomentMaxi System.out.println ("calculeCoteMomentMaxi : convergence a iteration n� = " +
           * i_ccmm + " cote moment = " + _e_cote_moment_maxi_ccmm_m + " m, butee = " + butee_Xm_cmf_t_ + " t, moment = " +
           * moment_Xm_tm_ + "tm"); //
           */
          break; // sortie boucle
        } // fin delta_prof_fiche <= prec_prof_m_
        else if (Math.abs(_delta_prof_cote_ccmm_m) > PREC_PROF_M) {
          // non convergence prof fiche
          _e_cote_moment_maxi_ccmm_m = _cote_moment_ccmm_m;
        } // fin else if
      } // fin boucle for i_ccmm
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeCoteMomentMaxi

  /**
   * Methode calculeDuc104 : regroupe les appels successifs des differents modules : v 1.04.
   */
  public void calculeDuc104() {
    try {
      // appels methodes :
      // calcul de l'inertie : inertie_m4_
      calculeInertie(epaisseur_act_m_, diametre_m_, nb_tubes_, inertie_m4_);
      // calcul du moment maxi admissible : moment_maxi_adm_tm_
      calculeMomentMaxiAdm(diametre_m_, contrainte_adm_t_m2_, inertie_m4_, moment_maxi_adm_tm_);
      // calcul de la force maxi admissible : force_maxi_t_, cote et valeur
      // moment maxi :
      calculeForceMaxi(diametre_m_, moment_maxi_adm_tm_, force_maxi_t_, cote_moment_maxi_m_, moment_maxi_tm_,
          convergence_moment_);
      if (!convergence_moment_) {
        outNewLine();
        System.out.println(" Calcul force maxi non convergent pour diametre " + diametre_m_ + "m; contrainte "
            + contrainte_adm_t_m2_ + " t/m2; accostage n� " + (iacc + 1));
        outNewLine();
      } // fin if (convergence_moment_ == false)
      // calcul de la cote de la fiche :
      calculeCoteFiche(force_maxi_t_, cote_fiche_m_, convergence_fiche_);
      if (!convergence_fiche_) {
        outNewLine();
        System.out.println(" calcul fiche non convergent pour diametre " + diametre_m_ + "m; contrainte "
            + contrainte_adm_t_m2_ + " t/m2; accostage n� " + iacc);
        outNewLine();
      } // fin (convergence_fiche_ == false)
      // calcul de la fiche corrigee (* 1.2) : profondeur enterr�e :
      fiche_m_ = cotes_sup_m_[0] - cote_fiche_m_; // > 0, avant correction
      fiche12_m_ = (1.2 * fiche_m_); // fiche corrigee * 1.2
      cote_pied_m_ = cotes_sup_m_[0] - fiche12_m_; // cote pied
      // calcul poids duc : poids_duc_t_
      long_duc_m_ = cote_tete_m_ - cote_pied_m_;
      calculePoidsDuc(diametre_m_, (epaisseur_act_m_ + (epaisseur_cor_mm_ * 0.001)), long_duc_m_, poids_duc_t_);
      // calcul des fleches :
      calculeFleches(fiche_m_, force_maxi_t_, inertie_m4_, fleche_choc_m_, fleche_tete_m_);
      // calcul de l'energie des tubes : energie_tubes_tm_
      calculeEnergieTubes(force_maxi_t_, fleche_choc_m_, energie_tubes_tm_);
      // calcul de l'energie de la defense : energie_defense_tm_
      calculeEnergieDefense(force_maxi_t_, deflexion_pc_, energie_defense_tm_);
      // calcul energie totale : energie_totale_tm_
      energie_totale_tm_ = energie_tubes_tm_ + energie_defense_tm_;
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeDuc104

  /**
   * Methode calculeDuc : regroupe les appels successifs des differents modules v 1.05.
   */
  public void calculeDuc() {
    try {
      // appels methodes :
      // calcul de l'inertie : inertie_m4_
      calculeInertie(epaisseur_act_m_, diametre_m_, nb_tubes_, inertie_m4_);
      // calcul du moment maxi admissible : moment_maxi_adm_tm_
      calculeMomentMaxiAdm(diametre_m_, contrainte_adm_t_m2_, inertie_m4_, moment_maxi_adm_tm_);
      /*
       * System.out.println (" "); System.out.println (" "); System.out.println ("calculDuc v 1.05 : contrainte = " +
       * contrainte_adm_t_m2_ + " t/m2; accostage n� " + (iacc + 1) + ", diametre_m = " + diametre_m_ + " epaisseur_m = " +
       * epaisseur_act_m_ + " m, inertie_m4_" + inertie_m4_ + "m4, moment maxi. adm. tm = " + moment_maxi_adm_tm_);
       * System.out.println (" ");
       */
      // init. force : new procedure C Barou : dur dur : dichotomie par
      // intervalles / 2
      boolean convergence_energie_ = false;
      force_maxi_t_ = (moment_maxi_adm_tm_) / (1.5 * (cote_acc_m_ - cotes_sup_m_[0]));
      double _delta_force_cd_t = force_maxi_t_ * 0.55;
      double _delta_energie_cd_tm;
      // System.out.println (" init force en t � " + force_maxi_t_);
      // iteration sur la force :
      byte i_cd_f;
      for (i_cd_f = 1; i_cd_f < NB_ITER; i_cd_f++) {
        // nb_iter constante
        // calcul de la cote de la fiche : pt de contre-butee : moment nul
        calculeCoteFiche(force_maxi_t_, cote_fiche_m_, convergence_fiche_);
        if (!convergence_fiche_) {
          outNewLine();
          System.out.println(" calcul fiche non convergent pour force = " + force_maxi_t_ + " t. SORTIE ! ");
          break;
        } // fin (convergence_fiche_ == false)
        // calcul de la fiche corrigee (* 1.2) : profondeur enterr�e :
        fiche_m_ = cotes_sup_m_[0] - cote_fiche_m_; // > 0, avant correction
        fiche12_m_ = (1.2 * fiche_m_); // >0 : fiche corrigee * 1.2
        cote_pied_m_ = cotes_sup_m_[0] - fiche12_m_; // cote pied
        // calcul poids duc : poids_duc_t_
        long_duc_m_ = cote_tete_m_ - cote_pied_m_;
        calculePoidsDuc(diametre_m_, (epaisseur_act_m_ + (epaisseur_cor_mm_ * 0.001)), long_duc_m_, poids_duc_t_);
        // calcul des fleches :
        calculeFleches(fiche_m_, force_maxi_t_, inertie_m4_, fleche_choc_m_, fleche_tete_m_);
        // calcul de l'energie des tubes : energie_tubes_tm_
        calculeEnergieTubes(force_maxi_t_, fleche_choc_m_, energie_tubes_tm_);
        // calcul de l'energie de la defense : energie_defense_tm_
        calculeEnergieDefense(force_maxi_t_, deflexion_pc_, energie_defense_tm_);
        // calcul energie totale : energie_totale_tm_
        energie_totale_tm_ = energie_tubes_tm_ + energie_defense_tm_;
        // System.out.println ("fleche choc = " + fleche_choc_m_ + " m;
        // �nergie_totale_tm_ = " +
        // energie_totale_tm_);
        // convergence energie :
        _delta_energie_cd_tm = energie_totale_tm_ - energie_acc_tm_;
        // System.out.println ("calcule force : delta �nergie tm = " +
        // _delta_energie_cd_tm);
        // System.out.println (" iteration force n� " + i_cd_f + " : force = " +
        // force_maxi_t_ + "
        // t, energie totale = " + energie_totale_tm_ + " tm, fiche = " +
        // fiche_m_ + " m" );
        // verification convergence :
        if (Math.abs(_delta_energie_cd_tm) <= PREC_ENERGIE_TM) {
          // precision : constante
          // convergence energie :
          convergence_energie_ = true;
          /*
           * System.out.println ("convergence force/energie iteration force n� " + i_cd_f + " energie tot = " +
           * energie_totale_tm_ + " tm, force = " + force_maxi_t_ + " t"); System.out.println("cote fiche = " +
           * cote_fiche_m_ + " m, longueur = " + long_duc_m_ + " m, poids = " + poids_duc_t_ + " t, fleche choc = " +
           * fleche_choc_m_ + " m"); System.out.println (" "); break;
           */
        } // fin if (_delta_energie_cd_tm <= prec_energie_tm_)
        else if (Math.abs(_delta_energie_cd_tm) > PREC_ENERGIE_TM) {
          // non convergence : poursuite iteration force : methode dichotomie
          // pat intervalles : 2
          if (energie_totale_tm_ < energie_acc_tm_) {
            force_maxi_t_ = force_maxi_t_ + _delta_force_cd_t;
          } else if (energie_totale_tm_ > energie_acc_tm_) {
            force_maxi_t_ = force_maxi_t_ - _delta_force_cd_t;
          }
          // division intervalle par 2 :
          _delta_force_cd_t = _delta_force_cd_t * 0.5;
          /*
           * ancienne methode : _delta_force_cd_t = (35 *(energie_acc_tm_ - energie_totale_tm_)) / (Math.sqrt(i_cd_f)*
           * fiche_m_); // test securitaire if (Math.abs(_delta_force_cd_t) > (force_maxi_t_ * 0.5)) {System.out.println ("
           * attention : delta force : " + _delta_force_cd_t + " t ramen� � 0.5 force"); _delta_force_cd_t =
           * force_maxi_t_ * 0.5 * Math.abs(_delta_force_cd_t)/(_delta_force_cd_t); } // fin if math
           */
        } // fin else if (_delta_energie_cd_tm > prec_energie_tm_)
      } // fin boucle force for i_cd_f
      if (!convergence_energie_) {
        outNewLine();
        System.out.println(" calcul force/energie non convergent pour diametre " + diametre_m_ + "m; epaisseur = "
            + epaisseur_act_m_ + " m");
        outNewLine();
      }
      // calcul cote maxi moment_maxi_tm_ :
      calculeCoteMomentMaxi(cote_fiche_m_, force_maxi_t_, moment_maxi_tm_, cote_moment_maxi_m_, convergence_cote_);
      if (!convergence_cote_) {
        outNewLine();
        System.out.println("Calcul cote moment maxi non convergent pour cote fiche = " + cote_fiche_m_ + " m, force = "
            + force_maxi_t_ + " t");
        System.out.println(" FIN DU CALCUL  : SORTIE ");
      } // fin (convergence_cote_ == false)
      // else if (convergence_cote_) {
      /*
       * System.out.println (" "); System.out.println ("Calcul cote et valeur moment maxi convergents dans ccmm pour
       * force = " + force_maxi_t_ + " t, energie = " + energie_totale_tm_ + " tm, moment maxi = " + moment_maxi_tm_ + "
       * tm a la cote " + cote_moment_maxi_m_ + " m"); System.out.println (" ");
       */
      // } // fin else
    } // fin try
    catch (final Exception e) {}
  } // fin methode calculeDuc v 1.05
}